//
//  WRLoginViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@protocol WRLoginViewControllerDelegate  <NSObject>

@optional

-(void)loginSuccessLoadNextViewController;

@end

@interface WRLoginViewController : WRBaseViewController

//@property (weak, nonatomic) IBOutlet UIButton *fbLoginButton;
@property (weak, nonatomic) IBOutlet UIView *facebookView;
@property (nonatomic, assign) BOOL isFromBiking;
@property (nonatomic, assign) BOOL isFromBikation;

@property (nonatomic, weak) id <WRLoginViewControllerDelegate> delegate;
@end
