//
//  WRForgotPasswordViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 20/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRForgotPasswordViewController.h"

@interface WRForgotPasswordViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *signInLabel;

@end

@implementation WRForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadCustomNavigationViewWithTitle:@""];
    // Do any additional setup after loading the view.
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    UITapGestureRecognizer *signInTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signInButtonTapped)];
    [self.signInLabel addGestureRecognizer:signInTap];
}



#pragma mark UIButton/ Tap Action Methods


- (IBAction)sendButtonAction:(id)sender
{
    if ([self.emailTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if(![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        [self forgotPasswordWebServiceMethod];
    }
}

-(void)forgotPasswordWebServiceMethod
{
    [self loadMBProgressCustomLoaderView];
    NSDictionary *params = @{@"email":self.emailTextField.text};
    [self.networkManager startGETRequestWithAPI:aForgotPassword andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        int status = [[[responseDictionary objectForKey:@"result"] objectForKey:@"status_code"] intValue];
        NSString *msgStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
        if (status == 200) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"Please check your email to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msgStr delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

-(void)signInButtonTapped
{
    [self customNavCloseButtonTapped];
}

#pragma mark - UITextFieldDelegate Methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (IS_IPHONE_6_PLUS) {
        [self.view changedYAxisTo:-70];
    }
    else
    {
        [self.view changedYAxisTo:-60];
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.view changedYAxisTo:0];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view changedYAxisTo:0];
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
