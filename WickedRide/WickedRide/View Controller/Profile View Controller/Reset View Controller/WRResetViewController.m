//
//  WRResetViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 04/12/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRResetViewController.h"

@interface WRResetViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *nPaswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@end

@implementation WRResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadCustomNavigationViewWithTitle:[@"Change Password" uppercaseString]];
    self.customNavView.closeButton.hidden = YES;
    self.customNavView.backButton.hidden = NO;
//    [self showNavigationViewWithBackButtonWithTitle:[@"Reset Password" uppercaseString]];
    [self configureViewWithDetails];
}

-(void)configureViewWithDetails
{
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
 
    self.nPaswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.nPaswordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];

    self.confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.confirmPasswordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


#pragma mark - UITextFieldDelegate Method

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (IS_IPHONE_5_OR_BELLOW) {
        if (textField == self.nPaswordTextField || textField == self.confirmPasswordTextField) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.view changedYAxisTo:-100];
            }];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.2 animations:^{
        [self.view changedYAxisTo:0];
    }];

    if (textField == self.passwordTextField) {
        [self.nPaswordTextField becomeFirstResponder];
    }
    else if (textField == self.nPaswordTextField)
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    
    
    [textField resignFirstResponder];
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound) {
//        return NO;
//    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton/ Tap Action

- (IBAction)resetButtonAction:(id)sender
{
    if ([self.passwordTextField.text isEmptyString]||[self.nPaswordTextField.text isEmptyString]||[self.confirmPasswordTextField.text isEmptyString]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please fill all the fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (self.nPaswordTextField.text.length<5)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Password must be at least 5 characters" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if(![self.nPaswordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Password mismatch" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSDictionary *params = @{@"email":self.userManager.user.emailId,
                                 @"old_password":self.passwordTextField.text,
                                 @"new_password":self.nPaswordTextField.text};
        [self loadMBProgressCustomLoaderView];
        [self.networkManager startPOSTRequestWithAPI:aResetPassword andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            [self hideMBProgressCutomLoader];
            NSString *msgStr;
            if ([[responseDictionary objectForKey:@"result"] objectForKey:@"data"]==nil) {
                msgStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"message"];
            }
            else
            {
                msgStr = [[responseDictionary objectForKey:@"result"] objectForKey:@"data"];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msgStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        } andFailure:^(NSString *errorMessage) {
            [self hideMBProgressCutomLoader];
            if (![errorMessage isEqualToString:kErrorMessage]) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
