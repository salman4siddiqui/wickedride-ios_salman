//
//  WRSignUpViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 14/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@protocol WRSignUpViewControllerDelegate <NSObject>

@optional

-(void)registrationIsSuccessfull;

@end

@interface WRSignUpViewController : WRBaseViewController

@property (nonatomic, weak) id <WRSignUpViewControllerDelegate> delegate;

@end
