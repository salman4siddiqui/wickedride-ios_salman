//
//  WRCropImageViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 17/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRCropImageViewControllerDelegate <NSObject>

@optional

-(void)imageAfterCroping:(UIImage *)croppedImage;

@end

@interface WRCropImageViewController : UIViewController

@property (nonatomic, strong) UIImage *selectedImage;

@property (nonatomic, weak) id <WRCropImageViewControllerDelegate>delegate;

@end
