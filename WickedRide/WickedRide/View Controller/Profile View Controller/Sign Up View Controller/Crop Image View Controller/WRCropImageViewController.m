//
//  WRCropImageViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 17/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCropImageViewController.h"
#import <BABCropperView.h>


@interface WRCropImageViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet BABCropperView *cropperView;
@property (weak, nonatomic) IBOutlet UIImageView *croppedImageView;
@property (weak, nonatomic) IBOutlet UIButton *cropButton;

@end

@implementation WRCropImageViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.cropButton.layer.cornerRadius = 3.0;
    self.cropButton.clipsToBounds = YES;
    
    self.cancelButton.layer.cornerRadius = 3.0;
    self.cancelButton.clipsToBounds = YES;
    
    self.cropperView.cropSize = CGSizeMake(640.0f, 640.0f);
    self.cropperView.image = self.selectedImage;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
}

- (void)showImagePicker {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
    [self.cropButton setTitle:@"Crop Image" forState:UIControlStateNormal];
}

#pragma mark - Button Targets

- (IBAction)cropButtonPressed:(id)sender {
    __weak typeof(self)weakSelf = self;
    
    self.cropperView.hidden = NO;
    self.croppedImageView.hidden = YES;
    [self.cropperView renderCroppedImage:^(UIImage *croppedImage, CGRect cropRect) {
        [weakSelf.delegate imageAfterCroping:croppedImage];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
    
}

- (IBAction)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)displayCroppedImage:(UIImage *)croppedImage {
    
    self.cropperView.hidden = YES;
    self.croppedImageView.hidden = NO;
    self.croppedImageView.image = croppedImage;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.cropperView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
