 //
//  WRProfileViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 26/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRProfileViewController.h"
#import "WRBikesCurrentBookingView.h"
#import "WREditProfileViewController.h"
#import "WREventsDetailView.h"
#import "WRBikation.h"
#import "WRBooking.h"
#import "WRReviewViewController.h"
#import <UIImageView+AFNetworking.h>

@interface WRProfileViewController ()<UIScrollViewDelegate, WREventsDetailViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIView *bikesView;
@property (weak, nonatomic) IBOutlet UIView *bikesSeperatorView;

@property (weak, nonatomic) IBOutlet UIView *eventsView;
@property (weak, nonatomic) IBOutlet UIView *eventsSeperatorView;
@property (weak, nonatomic) IBOutlet UIView *subscriptionView;
@property (weak, nonatomic) IBOutlet UILabel *availableRidesLabel;
@property (weak, nonatomic) IBOutlet UILabel *subscriptionLabel;


@property (nonatomic, assign) BOOL isBikesViewSelected;

@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIScrollView *bikeBookingScrollView;
@property (nonatomic, strong) UIScrollView *eventsScrollView;
@property (nonatomic, strong) UIScrollView *attendedEventsScrollView;

@property (nonatomic, strong) NSMutableArray *upcomingEventsArray;
@property (nonatomic, strong) NSMutableArray *attendedEventsArray;

@property (nonatomic, strong) NSMutableArray *bookingsArray;


@property (nonatomic, strong) UIView *bikeHeaderView;
@property (nonatomic, strong) UIView *eventHeaderView;

@property (nonatomic, strong) UIView *eventScrollDetailView;
@property (nonatomic, strong) UIView *bikesScrollDetailView;
@property (nonatomic, strong) NSDictionary *subscriptionDict;

@property (nonatomic, strong) UIPageControl *attendedPageControl;
@property (nonatomic, strong) UIPageControl *upcomingPageControl;
@property (nonatomic, strong) UIPageControl *bikePageControl;
@end

@implementation WRProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showNavigationMenuAndEditButtonForProfileScreenWithTitle:@"PROFILE"];
    self.upcomingEventsArray = [NSMutableArray array];
    self.attendedEventsArray = [NSMutableArray array];
    self.bookingsArray = [NSMutableArray array];
    
    
    [self addGestureToViewAndConfigureProperties];
    [self addContainerScrollView];
    [self callWebServiceToGetUserInformation];
    [self loadBikeScrollViewTitle];
    [self loadEventScrollViewTitle];
    self.eventHeaderView.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    if (![self.userManager.user.profileImageUrl isEmptyString]) {
        NSString *imageUrl = self.userManager.user.profileImageUrl;
        UIImage *placeholderImg = [UIImage imageNamed:@"user_profile"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        
        __weak UIImageView *weakImg = self.profileImageView;
        
        [self.profileImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
         }];
        
    }
}

#pragma mark - Add Custom bike header and event header view

-(void)loadBikeScrollViewTitle
{
    self.bikeHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 160, self.view.frame.size.width*2, 40)];
    UIView *currentBookingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [currentBookingView addSubview:[self returnLabelWithText:@"Current Booking" withXAxis:0]];
    UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 40, 40)];
    [nextButton setImage:[UIImage imageNamed:@"front-arrow-black"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(bikeViewNextButtonTapped) forControlEvents:UIControlEventTouchDown];
    
    [currentBookingView addSubview:nextButton];
    [self.bikeHeaderView addSubview:currentBookingView];
    
    UIView *subscriptionView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, 40)];
    [subscriptionView addSubview:[self returnLabelWithText:@"Subscription" withXAxis:0]];
    UIButton *previousButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 40, 40)];
    
    [previousButton setImage:[UIImage imageNamed:@"back-arrow-black"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(bikeViewPreviousButtonTapped) forControlEvents:UIControlEventTouchDown];
    [subscriptionView addSubview:previousButton];
    
    [self.bikeHeaderView addSubview:subscriptionView];

    [self.containerView addSubview:self.bikeHeaderView];
}

-(void)loadEventScrollViewTitle
{
    self.eventHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 160, self.view.frame.size.width*2, 40)];
    UIView *upcomingEventView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [upcomingEventView addSubview:[self returnLabelWithText:@"Upcoming event" withXAxis:0]];
    UIButton *nextButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-50, 0, 40, 40)];
    [nextButton setImage:[UIImage imageNamed:@"front-arrow-black"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(eventViewNextButtonTapped) forControlEvents:UIControlEventTouchDown];
    
    [upcomingEventView addSubview:nextButton];
    [self.eventHeaderView addSubview:upcomingEventView];
    
    UIView *attendedEventView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, 40)];
    [attendedEventView addSubview:[self returnLabelWithText:@"Attended event" withXAxis:0]];
    UIButton *previousButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 40, 40)];
    
    [previousButton setImage:[UIImage imageNamed:@"back-arrow-black"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(eventViewPreviousButtonTapped) forControlEvents:UIControlEventTouchDown];
    [attendedEventView addSubview:previousButton];
    
    [self.eventHeaderView addSubview:attendedEventView];
    
    [self.containerView addSubview:self.eventHeaderView];
}



-(UILabel *)returnLabelWithText:(NSString *)titleText withXAxis:(CGFloat )xAxis
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    label.font = MONTSERRAT_BOLD(16.0);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = titleText;
    return label;
}



#pragma mark - Web service methods

-(void)callWebServiceToGetUserInformation
{
    self.containerView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];
    [self.networkManager startGETRequestWithAPI:aProfileDetails andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        [self loadUserProfileSubscriptionAndCurrentBooking:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }];
}

-(void)loadUserProfileSubscriptionAndCurrentBooking:(NSDictionary *)responseDictionary
{
    self.subscriptionDict = [responseDictionary objectForKey:@"subscription"];
    
    for (NSDictionary *dictionary in [[responseDictionary objectForKey:@"bikations"] objectForKey:@"upcoming"]) {
        WRBikation *bikation = [WRBikation getBikationInformationFrom:[dictionary objectForKey:@"bikation"]];
        [self.upcomingEventsArray addObject:bikation];
    }
    for (NSDictionary *dictionary in [[responseDictionary objectForKey:@"bikations"] objectForKey:@"attended"]) {
        WRBikation *bikation = [WRBikation getBikationInformationFrom:[dictionary objectForKey:@"bikation"]];
        [self.attendedEventsArray addObject:bikation];
    }

    for (NSDictionary *dictionary in [responseDictionary objectForKey:@"bookings"]) {
        WRBooking *bookingObj = [WRBooking getBookingInformationFrom:dictionary];
        [self.bookingsArray addObject:bookingObj];
    }
    
    [self loadEventDetailsScrollView];
    [self loadAttendedEventScrollView];
    [self loadBikesScrollView];
    
    self.eventScrollDetailView = [[UIView alloc] initWithFrame:CGRectMake(0, 220, self.view.frame.size.width*2, 462)];
    [self.eventScrollDetailView addSubview:self.eventsScrollView];
    [self.eventScrollDetailView addSubview:self.attendedEventsScrollView];
    [self.eventScrollDetailView addSubview:self.upcomingPageControl];
    [self.eventScrollDetailView addSubview:self.attendedPageControl];
    [self.containerView addSubview:self.eventScrollDetailView];
    
    if (self.subscriptionDict.count) {
        self.bikesScrollDetailView = [[UIView alloc] initWithFrame:CGRectMake(0, 220, self.view.frame.size.width*2, 462)];
        
        self.availableRidesLabel.text = [NSString stringWithFormat:@"%@/%@ rides",[self.subscriptionDict objectForKey:@"rides_available"],[self.subscriptionDict objectForKey:@"rides_total"]];
        self.subscriptionLabel.text = [NSString stringWithFormat:@"Your %@ subscription expires on %@",[[self.subscriptionDict objectForKey:@"subscription_type"] lowercaseString],[[self.subscriptionDict objectForKey:@"subscription_expiry_date"] convertDateIntoStringForClock]];
        
        [self.subscriptionView changedXAxisTo:self.view.frame.size.width andYAxisTo:0];
        [self.subscriptionView changeWidthTo:self.view.frame.size.width andHeightTo:self.view.frame.size.height-240];
        [self.bikesScrollDetailView addSubview:self.subscriptionView];
    }
    else
    {
        self.bikesScrollDetailView = [[UIView alloc] initWithFrame:CGRectMake(0, 220, self.view.frame.size.width, 462)];
    }
    [self.bikesScrollDetailView addSubview:self.bikeBookingScrollView];
    [self.bikesScrollDetailView addSubview:self.bikePageControl];
    [self.containerView addSubview:self.bikesScrollDetailView];

    
    self.containerView.hidden = NO;
    self.bikeBookingScrollView.hidden = NO;
    self.eventScrollDetailView.hidden = YES;

}

#pragma mark Views Custom Methods

-(void)addGestureToViewAndConfigureProperties
{
    self.isBikesViewSelected = YES;
    self.eventsSeperatorView.hidden = YES;
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;
    
    UITapGestureRecognizer *bikeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bikesViewTapped)];
    [self.bikesView addGestureRecognizer:bikeTap];
    
    UITapGestureRecognizer *eventTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(eventsViewTapped)];
    [self.eventsView addGestureRecognizer:eventTap];
}

-(void)addContainerScrollView
{
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.containerView.frame.size.height);
    self.mainScrollView.contentSize = CGSizeMake(320, 720.0);
    [self.mainScrollView addSubview:self.containerView];
    [self.view addSubview:self.mainScrollView];
}


-(void)loadBikesScrollView
{
    self.bikeBookingScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 432)];
    self.bikeBookingScrollView.pagingEnabled = YES;
    self.bikeBookingScrollView.delegate = self;
    
    if (self.bookingsArray.count) {
        CGFloat width = 0;
        
        self.bikePageControl = [[UIPageControl alloc] init];
        self.bikePageControl.frame = CGRectMake(0, 0, self.bookingsArray.count*20, 30);
        self.bikePageControl.numberOfPages = self.bookingsArray.count;
        self.bikePageControl.userInteractionEnabled = NO;
        self.bikePageControl.center = CGPointMake(self.bikeBookingScrollView.center.x, 0);
        self.bikePageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        self.bikePageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        
        for (WRBooking *booking in self.bookingsArray) {
            WRBikesCurrentBookingView *bookingDetailView = [[[NSBundle mainBundle] loadNibNamed:kBikesCurrentBookingViewID owner:self options:nil] firstObject];
            bookingDetailView.translatesAutoresizingMaskIntoConstraints = YES;
            bookingDetailView.frame = CGRectMake(width, 30, self.view.frame.size.width, 432);
            [bookingDetailView configureViewWithBookingDetails:booking];
            [self.bikeBookingScrollView addSubview:bookingDetailView];
            width+=self.view.frame.size.width;
        }
        self.bikeBookingScrollView.contentSize = CGSizeMake(self.bookingsArray.count*self.view.frame.size.width, 430) ;
    }
    else
    {
        WRBikesCurrentBookingView *bookingDetailView = [[[NSBundle mainBundle] loadNibNamed:kBikesCurrentBookingViewID owner:self options:nil] firstObject];
        bookingDetailView.translatesAutoresizingMaskIntoConstraints = YES;
        bookingDetailView.frame = CGRectMake(0, 0, self.view.frame.size.width, 432);
        [bookingDetailView configureViewWithBookingDetails:nil];
        [self.bikeBookingScrollView addSubview:bookingDetailView];
    }
}

-(void)loadAttendedEventScrollView
{
    self.attendedEventsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, 432)];
    self.attendedEventsScrollView.pagingEnabled = YES;
    self.attendedEventsScrollView.delegate = self;
    
    if (self.attendedEventsArray.count) {
        CGFloat width = 0;
        self.attendedPageControl = [[UIPageControl alloc] init];
        self.attendedPageControl.frame = CGRectMake(0, 0, self.attendedEventsArray.count*20, 30);
        self.attendedPageControl.numberOfPages = self.attendedEventsArray.count;
        self.attendedPageControl.userInteractionEnabled = NO;
        self.attendedPageControl.center = CGPointMake(self.attendedEventsScrollView.center.x, 0);
        self.attendedPageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        self.attendedPageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        
        for (WRBikation *bikation in self.attendedEventsArray) {
            WREventsDetailView *eventsDetailView = [[[NSBundle mainBundle] loadNibNamed:kEventsDetailViewID owner:self options:nil] firstObject];
            eventsDetailView.translatesAutoresizingMaskIntoConstraints = YES;
            eventsDetailView.frame = CGRectMake(width, 30, self.view.frame.size.width, 432);
            eventsDetailView.delegate = self;
            [eventsDetailView configureViewWithBookingDetails:bikation andIsAttended:YES];
            [self.attendedEventsScrollView addSubview:eventsDetailView];
            width+=self.view.frame.size.width;
        }
        self.attendedEventsScrollView.contentSize = CGSizeMake(self.attendedEventsArray.count*self.view.frame.size.width, 430) ;
    }
    else
    {
        WREventsDetailView *eventsDetailView = [[[NSBundle mainBundle] loadNibNamed:kEventsDetailViewID owner:self options:nil] firstObject];
        eventsDetailView.translatesAutoresizingMaskIntoConstraints = YES;
        eventsDetailView.frame = CGRectMake(0, 0, self.view.frame.size.width, 432);
        [eventsDetailView configureViewWithBookingDetails:nil andIsAttended:YES];
        [self.attendedEventsScrollView addSubview:eventsDetailView];
    }

}

-(void)loadEventDetailsScrollView
{
    self.eventsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 432)];

    self.eventsScrollView.pagingEnabled = YES;
    self.eventsScrollView.delegate = self;
    
    if (self.upcomingEventsArray.count) {
        CGFloat width = 0;
        self.upcomingPageControl = [[UIPageControl alloc] init];
        self.upcomingPageControl.frame = CGRectMake(0, 0, self.upcomingEventsArray.count*20, 30);
        self.upcomingPageControl.numberOfPages = self.upcomingEventsArray.count;
        self.upcomingPageControl.userInteractionEnabled = NO;
        self.upcomingPageControl.center = CGPointMake(self.eventsScrollView.center.x, 0);
        self.upcomingPageControl.currentPageIndicatorTintColor = [UIColor blackColor];
        self.upcomingPageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        
        for (WRBikation *bikation in self.upcomingEventsArray) {
            WREventsDetailView *eventsDetailView = [[[NSBundle mainBundle] loadNibNamed:kEventsDetailViewID owner:self options:nil] firstObject];
            eventsDetailView.translatesAutoresizingMaskIntoConstraints = YES;
            eventsDetailView.frame = CGRectMake(width, 30, self.view.frame.size.width, 432);
            [eventsDetailView configureViewWithBookingDetails:bikation andIsAttended:NO];
            [self.eventsScrollView addSubview:eventsDetailView];
            width+=self.view.frame.size.width;
        }
        self.eventsScrollView.contentSize = CGSizeMake(self.upcomingEventsArray.count*self.view.frame.size.width, 430) ;
    }
    else
    {
        WREventsDetailView *eventsDetailView = [[[NSBundle mainBundle] loadNibNamed:kEventsDetailViewID owner:self options:nil] firstObject];
        eventsDetailView.translatesAutoresizingMaskIntoConstraints = YES;
        eventsDetailView.frame = CGRectMake(0, 0, self.view.frame.size.width, 432);
        [eventsDetailView configureViewWithBookingDetails:nil andIsAttended:NO];
        [self.eventsScrollView addSubview:eventsDetailView];
    }
    
}


-(UILabel *)returnScrollTitleLabelWithText:(NSString *)text withXAxis:(CGFloat )x
{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, self.view.frame.size.width, 70)];
    titleLabel.text = text;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = AVENIR_BLACK(18.0);
    titleLabel.textColor = [UIColor colorWithRed:0.11 green:0.11 blue:0.15 alpha:1];
    return titleLabel;
}


#pragma mark - UITap/ UIButton Action Methods

-(void)bikesViewTapped
{
    if (self.bikesScrollDetailView.frame.origin.x<0) {
        self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    }
    else
    {
        self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 720);
    }
    self.bikesSeperatorView.hidden = NO;
    self.eventsSeperatorView.hidden = YES;
    self.eventHeaderView.hidden = YES;
    self.bikeHeaderView.hidden = NO;
    
    self.eventScrollDetailView.hidden = YES;
    self.bikesScrollDetailView.hidden = NO;
}


-(void)eventsViewTapped
{
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 720);
    self.bikesSeperatorView.hidden = YES;
    self.eventsSeperatorView.hidden = NO;
    self.eventHeaderView.hidden = NO;
    self.bikeHeaderView.hidden = YES;

    self.eventScrollDetailView.hidden = NO;
    self.bikesScrollDetailView.hidden = YES;
}

-(void)bikeViewNextButtonTapped
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.bikeHeaderView changedXAxisTo:-self.view.frame.size.width];
        [self.bikesScrollDetailView changedXAxisTo:-self.view.frame.size.width];
    }];
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);

}

-(void)bikeViewPreviousButtonTapped
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.bikeHeaderView changedXAxisTo:0];
        [self.bikesScrollDetailView changedXAxisTo:0];
    }];
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 720);
}

-(void)eventViewNextButtonTapped
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.eventHeaderView changedXAxisTo:-self.view.frame.size.width];
        [self.eventScrollDetailView changedXAxisTo:-self.view.frame.size.width];
    }];
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 720);
}

-(void)eventViewPreviousButtonTapped
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.eventHeaderView changedXAxisTo:0];
        [self.self.eventScrollDetailView changedXAxisTo:0];
    }];
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 720);
    
}





-(IBAction)editProfileBarBtnTapped:(id)sender
{
    WREditProfileViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:kEditProfileViewControllerID];
    
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:editVC];
    [self.navigationController presentViewController:navCtr animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIScrollViewDelegate Method

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.attendedEventsScrollView )
    {
        int page = scrollView.contentOffset.x/self.view.frame.size.width;
        self.attendedPageControl.currentPage = page;
    }
    else if (scrollView == self.eventsScrollView )
    {
        int page = scrollView.contentOffset.x/self.view.frame.size.width;
        self.upcomingPageControl.currentPage = page;
    }
    else if (scrollView == self.bikeBookingScrollView)
    {
        int page = scrollView.contentOffset.x/self.view.frame.size.width;
        self.bikePageControl.currentPage = page;
    }

}

#pragma mark - WREventsDetailViewDelegate Methods

-(void)attendedLabelTappedWithSelectedBikation:(WRBikation *)selectedBikation
{
    WRReviewViewController *reviewVC = [self.storyboard instantiateViewControllerWithIdentifier:kReviewViewControllerID];
    reviewVC.selectedBikationObj = selectedBikation;
    [self.navigationController presentViewController:reviewVC animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
