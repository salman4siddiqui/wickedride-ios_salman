//
//  WRWalletViewController.m
//  WickedRide
//
//  Created by Boyapati Vinay Kumar on 16/06/17.
//  Copyright © 2017 Inkoniq. All rights reserved.
//

#import "WRWalletViewController.h"


#define kTitleStr @"WALLET"

@interface WRWalletViewController ()
@property (nonatomic, strong) NSDictionary *walletDic;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *refrerStringLbl;
@property (weak, nonatomic) IBOutlet UILabel *referalCodeLbl;

@end

@implementation WRWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showNavigationMenuForFilterScreenWithTitle:kTitleStr];
    [self callWebServiceToGetUserWallet];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Call web service for get user wallet balance

-(void)callWebServiceToGetUserWallet
{
    [self loadMBProgressCustomLoaderView];
    [self.networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]] forHTTPHeaderField:@"Authorization"];

    NSLog(@"token string %@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]);
    
    
    [self.networkManager GET:aCheckBalance parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideMBProgressCutomLoader];
        int status = (int)[[[responseObject objectForKey:@"result"] objectForKey:@"status_code"] integerValue];
        if (status == 200) {
            self.walletDic = [[responseObject objectForKey:@"result"] objectForKey:@"data"];
            [self displayUserWalletBalance:[[self.walletDic valueForKey:@"balance"] stringValue] referString:[self.walletDic valueForKey:@"refer_string"] referCode:[[NSUserDefaults standardUserDefaults] objectForKey:@kReferelCode]];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        if (![error.localizedDescription isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
    
}
-(void)displayUserWalletBalance:(NSString* )amount referString:(NSString *)referStr referCode:(NSString *) code{
    self.balanceLbl.text = amount;
    self.refrerStringLbl.text = referStr;
    self.referalCodeLbl.text = code;
}

// share to friends
- (IBAction)shareBtnAction:(id)sender
{

    NSString* title = [self.walletDic valueForKey:@"share_str"];
    NSString* link = @"https://itunes.apple.com/us/app/wicked-ride/id1121704128?ls=1&mt=8";
    NSArray* dataToShare = @[title, link];
    
   
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
