//
//  WRBaseViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "WRConstants.h"
#import "UIView+FrameChange.h"
#import "WRCustomNavView.h"
#import "NSString+DataValidator.h"
#import "WRLabel.h"
#import "WRUserManager.h"
#import "WRNetworkManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "WRSlideAnimatedTransitioning.h"

@interface WRBaseViewController : UIViewController

@property (strong, nonatomic) UIBarButtonItem *leftmenuBarButton;
@property (strong, nonatomic) UIBarButtonItem *nextBarButton;
@property (strong, nonatomic) UIBarButtonItem *backBarButton;
@property (nonatomic, strong) WRCustomNavView *customNavView;


@property (nonatomic, strong) WRUserManager *userManager;
@property (nonatomic, strong) WRNetworkManager *networkManager;

@property(nonatomic, strong) MBProgressHUD *loaderView;


//UINavigation Bar Custom Methods


-(void)showNavigationMenuForFilterScreenWithTitle: (NSString *)titleString andIsFromBikeList:(BOOL )isFromBikeList;
-(void)showNavigationViewWithBackButtonWithTitle:(NSString *)title;
-(void)showNavigationMenuForFilterScreenWithTitle: (NSString *)titleString;
-(void)showNavigationViewWithBackButtonWithAttributeTitle:(NSString *)title;
-(void)showNavigationMenuAndEditButtonForProfileScreenWithTitle: (NSString *)titleString;

- (IBAction)backBarButtonTapped:(id)sender;

-(void)loadCustonTitleNavigationViewWith:(NSString *)titleString;


-(void)callWebServiceToGetAllArea;
-(void)loadMBProgressCustomLoaderView;
-(void)hideMBProgressCutomLoader;

// Custom Navigation Bar Methods


-(void)loadCustomNavigationViewWithTitle:(NSString *)titleString;
-(void)customNavCloseButtonTapped;


// Animation Methods

-(void)viewZoomInEffect:(UIView *)aView;
-(void)viewZoomOutEffect:(UIView *)aView;

// Return date

-(NSDate *)returnDateFromString:(NSString *)dateStr;

-(void)addSoundAndVibrateMobileOnButtonAction;

@end
