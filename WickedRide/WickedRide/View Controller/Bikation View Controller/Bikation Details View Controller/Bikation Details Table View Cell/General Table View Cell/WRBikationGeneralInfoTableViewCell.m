//
//  WRBikationGeneralInfoTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationGeneralInfoTableViewCell.h"
#import "NSString+DataValidator.h"

@implementation WRBikationGeneralInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureViewWithBikationDetails:(WRBikationDetails *)bikationDetail
{
    switch (self.tag) {
        case 0:
        {
            [self.genralImageView setImage:[UIImage imageNamed:@"price"]];
            self.headerTitleLabel.text = [@"Price" uppercaseString];
            self.descriptionLabel.text = [NSString stringWithFormat:@"Rs %@/-",[bikationDetail.price getFormattedPrice]];
        }
            break;
        case 2:
        {
            [self.genralImageView setImage:[UIImage imageNamed:@"description"]];
            self.headerTitleLabel.text = [@"Description :" uppercaseString];
            self.descriptionLabel.text = bikationDetail.bikationDescription;
        }
            break;
        case 4:
        {
            [self.genralImageView setImage:[UIImage imageNamed:@"bike"]];
            self.headerTitleLabel.text = [@"Recomended bikes" uppercaseString];
            self.descriptionLabel.text =[[bikationDetail.recommendedBikes componentsJoinedByString:@", "] capitalizedString];
        }
            break;
            
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
