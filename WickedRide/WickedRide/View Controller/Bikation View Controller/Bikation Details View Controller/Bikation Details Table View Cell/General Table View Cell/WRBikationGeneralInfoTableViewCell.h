//
//  WRBikationGeneralInfoTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikationDetails.h"

@interface WRBikationGeneralInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *genralImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;


-(void)configureViewWithBikationDetails:(WRBikationDetails *)bikationDetail;

@end
