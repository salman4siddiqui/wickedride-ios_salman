//
//  WRBikationMoreInfoTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationMoreInfoTableViewCell.h"
#import "WRConstants.h"
#import "WRBikationMoreInfoView.h"
#import "WRFeatureInfo.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@interface WRBikationMoreInfoTableViewCell ()
{
    CGFloat cellWidth;
}

@property (nonatomic, strong)WRBikationDetails *selectedBikationObj;

@end

@implementation WRBikationMoreInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureViewWithBikationDetailsWithWidth:(CGFloat)width andBikationObj:(WRBikationDetails *)bikationDetails
{
    self.selectedBikationObj = bikationDetails;
    cellWidth = width;
    switch (self.tag) {
        case 0:
        {
            [self.moreInfoImageView setImage:[UIImage imageNamed:@"includes"]];
            self.moreInfoTitleLabel.text = [@"includes:" uppercaseString];
            [self configureIncludeCellWithBikationDetails];
        }
            break;
            
        case 1:
        {
            [self.moreInfoImageView setImage:[UIImage imageNamed:@"exclude"]];
            self.moreInfoTitleLabel.text = [@"excludes:" uppercaseString];
            [self configureExcludeCellWithBikationDetails];
        }
            break;

        default:
            break;
    }
}

-(void)configureIncludeCellWithBikationDetails
{
//    CGPoint point = CGPointMake(60, 120);
//    CGSize size = [self returnTextSize:@"More: Club goodies, Club memebership"];
//    [self.contentView addSubview:[self retutnViewWithPoint:point withTitleText:@"More: Club goodies, Club memebership" andImage:@"club" andSize:size]];

    CGSize size = CGSizeMake(0, 0);
    for (int i = 0; i<self.selectedBikationObj.inclusionArray.count; i++) {
        WRFeatureInfo *featureInfo = [self.selectedBikationObj.inclusionArray objectAtIndex:i];
        CGPoint point = CGPointMake(60, (i*size.height)+40);
        [self.contentView addSubview:[self retutnViewWithPoint:point withFeaturedObj:featureInfo]];
        size = [self returnTextSize:featureInfo.featureDescription];
    }
}

-(void)configureExcludeCellWithBikationDetails
{
    CGSize size = CGSizeMake(0, 0);
    for (int i = 0; i<self.selectedBikationObj.exculusionArray.count; i++) {
        WRFeatureInfo *featureInfo = [self.selectedBikationObj.exculusionArray objectAtIndex:i];
        CGPoint point = CGPointMake(60, (i*size.height)+40);
        [self.contentView addSubview:[self retutnViewWithPoint:point withFeaturedObj:featureInfo]];
        size = [self returnTextSize:featureInfo.featureDescription];
    }
}


-(CGSize )returnTextSize:(NSString *)moreInfoStr
{
    NSDictionary *attributes = @{NSFontAttributeName: AVENIR_ROMAN(15.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:moreInfoStr attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(cellWidth-110, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    return size;

}


-(UIView *)retutnViewWithPoint:(CGPoint )point withFeaturedObj:(WRFeatureInfo *)featuredInfo
{
    CGSize size = [self returnTextSize:featuredInfo.featureDescription];
    
    UIView *moreInfoView = [[UIView alloc] init];
    [moreInfoView setFrame:CGRectMake(point.x , point.y, size.width+45,(size.height<25)?25:size.height)];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    NSString *imageUrl = [featuredInfo.iconImageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = imageView;
    
    [imageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];

    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, size.width+5,(size.height<25)?20:size.height)];
    nameLabel.text = featuredInfo.featureDescription;
    nameLabel.font = AVENIR_ROMAN(15.0);
    nameLabel.numberOfLines = 0;
    nameLabel.textColor = [UIColor colorWithRed:0.47 green:0.47 blue:0.47 alpha:1];
                                                                   
    [moreInfoView addSubview:imageView];
    [moreInfoView addSubview:nameLabel];
    
    return moreInfoView;
}

@end
