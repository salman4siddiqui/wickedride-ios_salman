//
//  WRBikationViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationViewController.h"
#import "WRBikationCollectionViewCell.h"
#import "WRBikationDetailsViewController.h"
#import "WRAppDelegate.h"
#import "WRBikation.h"

@interface WRBikationViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *bikationCollectionView;

@property (nonatomic, strong) NSMutableArray *bikationArray;
@property (weak, nonatomic) IBOutlet UIImageView *bikationBackgroungImageView;

@end



@implementation WRBikationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.bikationArray = [NSMutableArray array];
    // Do any additional setup after loading the view.
    
    // Bikation removed for the first build
    self.navigationController.navigationBarHidden = YES;
    //    [self callWebServiceToGetBikationList];

    
    [self showNavigationMenuForFilterScreenWithTitle:@"YOUR RIDES"];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:IS_HIDE_LOCATION_VIEW])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_HIDE_LOCATION_VIEW];
    }
}

#pragma mark Web Service Methods

-(void)callWebServiceToGetBikationList
{
    self.bikationCollectionView.hidden = YES;
    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aBikationList andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
        
        for (NSDictionary *bikationDict in [[responseDictionary objectForKey:@"result"] objectForKey:@"data"]) {
            WRBikation *bikationObj = [WRBikation getBikationInformationFrom:bikationDict];
            [self.bikationArray addObject:bikationObj];
            
        }
        self.bikationCollectionView.hidden = NO;
        [self.bikationCollectionView reloadData];
        [self hideMBProgressCutomLoader];
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];
    
}



#pragma mark - UICollectionViewDataSource/UICollectionViewDelegate Methods

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.bikationArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WRBikation *bikationObj = [self.bikationArray objectAtIndex:indexPath.row];
    WRBikationCollectionViewCell *cell = (WRBikationCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kbikationCollectionViewCellID forIndexPath:indexPath];
    [cell configureCellWithSelectedBikationObj:bikationObj];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float aspectRatio = 230.0/335.0; //cell height/width;
    return CGSizeMake(collectionView.frame.size.width-40, (collectionView.frame.size.width-40)*aspectRatio);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WRBikation *bikationObj = [self.bikationArray objectAtIndex:indexPath.row];
    WRBikationDetailsViewController *bikationDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:kBikationDetailsViewControllerID];
    bikationDetailsVC.selectedBikationObj = bikationObj;
    [self.navigationController pushViewController:bikationDetailsVC animated:YES];
}


#pragma mark -- YSLTransitionAnimatorDataSource

- (UIImageView *)pushTransitionImageView
{
    WRBikationCollectionViewCell *cell = (WRBikationCollectionViewCell *)[self.bikationCollectionView cellForItemAtIndexPath:[[self.bikationCollectionView indexPathsForSelectedItems] firstObject]];
    return cell.bikeImageView;
}

- (UIImageView *)popTransitionImageView
{
    return nil;
}


#pragma mark - UIButton/ Tap Actions

- (IBAction)menuButtonAction:(id)sender
{
    WRAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate showLeftMenuViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
