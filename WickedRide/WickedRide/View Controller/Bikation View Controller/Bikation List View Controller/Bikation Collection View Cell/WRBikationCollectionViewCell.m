//
//  WRBikationCollectionViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikationCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@interface WRBikationCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *conductLabel;

@end

@implementation WRBikationCollectionViewCell

-(void)configureCellWithSelectedBikationObj:(WRBikation *)bikationObj
{
    self.nameLabel.text = bikationObj.bikationTitle;
    self.distanceLabel.text = bikationObj.distance;
    self.dateLabel.text = [NSString stringWithFormat:@"%@ (%@)",bikationObj.startDate,bikationObj.duration];
    self.conductLabel.text = bikationObj.conductorName;
    NSString *imageUrl = [bikationObj.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.bikeImageView;
    
    [self.bikeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];

}

@end
