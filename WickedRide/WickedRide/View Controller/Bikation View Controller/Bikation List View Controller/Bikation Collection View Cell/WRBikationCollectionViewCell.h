//
//  WRBikationCollectionViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikation.h"

@interface WRBikationCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bikeImageView;

-(void)configureCellWithSelectedBikationObj:(WRBikation *)bikationObj;

@end
