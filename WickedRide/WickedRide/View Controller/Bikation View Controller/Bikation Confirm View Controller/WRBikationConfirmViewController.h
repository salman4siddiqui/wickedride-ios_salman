//
//  WRBikationConfirmViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 26/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBikationDetails.h"
#import "WRBikation.h"
#import "PaymentsSDK.h"


@interface WRBikationConfirmViewController : WRBaseViewController< PGTransactionDelegate >

@property (nonatomic, strong) WRBikationDetails *selectedBikationDetailObj;
@property (nonatomic, strong) WRBikation *selectedBikation;
@end
