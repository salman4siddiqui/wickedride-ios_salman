//
//  WRReviewViewController.h
//  WickedRide
//
//  Created by Ajith kumar on 12/29/15.
//  Copyright © 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBikation.h"

@interface WRReviewViewController : WRBaseViewController

@property (nonatomic, strong) WRBikation *selectedBikationObj;

@end
