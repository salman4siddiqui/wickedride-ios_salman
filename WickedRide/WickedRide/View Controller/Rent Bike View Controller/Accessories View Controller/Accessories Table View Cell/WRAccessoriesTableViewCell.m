//
//  WRAccessoriesTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRAccessoriesTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@interface WRAccessoriesTableViewCell ()

@property (nonatomic, assign) BOOL isCheckBoxSelected;

@end

@implementation WRAccessoriesTableViewCell

- (void)awakeFromNib {
    // Initialization code
      self.isCheckBoxSelected = YES;

}

-(void)configureAccessoryCellWith:(WRAccessories *)accessory
{
    NSString *imageUrl = [accessory.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.accessoryImageView;
    
    [self.accessoryImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
    self.nameLabel.text = accessory.name;
    self.descriptionLabel.text = accessory.accessoryDescription;
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/hour",accessory.rentalAmount];;
}

#pragma mark - UIButton Actions

- (IBAction)checkBoxButtonAction:(id)sender
{
    
    if (self.isCheckBoxSelected)
    {
        [self.checkBoxButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
    }
    else
    {
        [self.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }
    [self.delegate checkBoxButtonTappedWith:self.tag andIsCheckBoxSelected:self.isCheckBoxSelected];
    
    self.isCheckBoxSelected = !self.isCheckBoxSelected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
