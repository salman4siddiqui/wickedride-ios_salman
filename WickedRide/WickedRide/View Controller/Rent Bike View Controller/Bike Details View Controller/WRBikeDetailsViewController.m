//
//  WRBikeDetailsViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 20/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeDetailsViewController.h"
#import "WRBikeDetailsTableViewCell.h"
#import "WRBikeDetailBookNowTableViewCell.h"
#import "WRReservationViewController.h"
#import "WRBikeDetails.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "WRArea.h"
#import "WRLoginViewController.h"
#import "QGSdk.h"

@interface WRBikeDetailsViewController ()<UITableViewDataSource, UITableViewDelegate, WRBikeDetailBookNowTableViewCellDelegate, WRLoginViewControllerDelegate, UIAlertViewDelegate>
{
    int selectedArea;
}

@property (weak, nonatomic) IBOutlet UIView *topBikeView;

@property (weak, nonatomic) IBOutlet UIView *makeLogoView;
@property (weak, nonatomic) IBOutlet UIImageView *makeImageView;
@property (weak, nonatomic) IBOutlet UITableView *bikeDetailsTableView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UILabel *bikeNameLabel;
@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *bikeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;

@property (weak, nonatomic) IBOutlet UIImageView *locationDropDownImage;
@property (weak, nonatomic) IBOutlet UILabel *effectivePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentalPriceLabel;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@property (nonatomic, strong) WRBikeDetails *bikeDetailsObj;
@property (weak, nonatomic) IBOutlet UIView *bikeInfoView;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (nonatomic, strong) UIScrollView *mainScrollView;
@end

@implementation WRBikeDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    self.bikeDetailsTableView.scrollEnabled = NO;
    selectedArea = 0;
    // header
    // If you're using a xib and storyboard. Be sure to specify the frame
    
    
    // custom navigation left item
    [self changeViewFrames];
    [self callWebServiceToGetBikeDetails];
    [self addShadeToBackButton];
    [self addContainerScrollView];
    [self loadBikeImageView];
    
    
}




#pragma mark Custom Method of the view

-(void)changeViewFrames
{
    self.bikeInfoView.hidden = YES;
    self.priceView.hidden = YES;
    self.bottomView.hidden = YES;
    self.makeImageView.hidden = YES;
    UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loactionViewTapped)];
    [self.locationView addGestureRecognizer:locationTap];
    
    self.makeLogoView.layer.cornerRadius = self.makeLogoView.frame.size.width/2;
    self.makeLogoView.clipsToBounds = YES;
    
    CGFloat aspectRatio = 315.0/375.0;
    CGFloat bikeAspectRatio = 278.0/375.0;
    CGRect rect = [UIScreen mainScreen].bounds;
    
    self.topBikeView.translatesAutoresizingMaskIntoConstraints = YES;
    self.topBikeView.frame = CGRectMake(0,0, rect.size.width,  self.view.frame.size.width*aspectRatio);
    
    self.bikeImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.bikeImageView.frame = CGRectMake(0,0, rect.size.width,  self.view.frame.size.width*bikeAspectRatio);
}

-(void)addContainerScrollView 
{

    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
     self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x , self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-100)];
    if (IS_IPHONE_5_OR_BELLOW) {
        self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 468.0);
        self.mainScrollView.contentSize = CGSizeMake(320, 468.0);
    }
    else
    {
        self.containerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100);
        self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-100);
    }
    [self.mainScrollView addSubview:self.containerView];
    [self.view addSubview:self.mainScrollView];
    [self.view bringSubviewToFront:self.customNavView];
}

-(void)addShadeToBackButton
{
//    [self.backButton.layer setShadowColor:[UIColor blackColor].CGColor];
//    [self.backButton.layer setShadowOpacity:0.5];
//    [self.backButton.layer setShadowRadius:2.0];
//    [self.backButton.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    self.locationTableView.layer.borderWidth = 1.0;
    self.locationTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

-(void)loadBikeImageView
{
    NSString *imageUrl = [self.selectedBikeObj.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.bikeImageView;
    
    [self.bikeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.bikeDetailsTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);

}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark Web Service Methods

-(void)callWebServiceToGetBikeDetails
{
    [self loadMBProgressCustomLoaderView];
    NSString *url = [NSString stringWithFormat:@"%@/%@",aBikeDetails,self.selectedBikeObj.bikeId];
    self.containerView.hidden = YES;
    NSString *city_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kSelectedLocation] objectForKey:@"id"]];

    NSDictionary *params;
    if ([self.paramsDict objectForKey:@"start_date"] !=nil) {
        params = @{@"city_id":city_id,
                   @"start_date":[self.paramsDict objectForKey:@"start_date"],
                   @"start_time":[self.paramsDict objectForKey:@"start_time"],
                   @"end_date":[self.paramsDict objectForKey:@"end_date"],
                   @"end_time":[self.paramsDict objectForKey:@"end_time"]};
    }
    else
    {
        params = @{@"city_id":city_id};
    }
    
    
    [self.networkManager startGETRequestWithAPI:url andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        self.bikeDetailsObj = [WRBikeDetails getBikeDetailsInformationFrom:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        [self.bikeDetailsTableView reloadData];
        
        if ([self.paramsDict objectForKey:@"area_id"]!=nil) {
            for (int i = 0; i<self.bikeDetailsObj.availabelAreaArray.count; i++) {
                WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:i];
                if ([[self.paramsDict objectForKey:@"area_id"] isEqualToString:@"1"]) {
                    if (area.isBikeAvailabel == YES) {
                        selectedArea = i;
                    }
                }
                else
                {
                    if ([[self.paramsDict objectForKey:@"area_id"] isEqualToString:area.area_id]) {
                        selectedArea = i;
                    }
                    else
                    {
                        selectedArea = 0;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i<self.bikeDetailsObj.availabelAreaArray.count; i++) {
                WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:i];
                    if (area.isBikeAvailabel == YES) {
                        if (selectedArea == 0) {
                            selectedArea = i;

                        }
                    }
                
            }
        }
        
        
        
        [self loadViewDetails];
        self.containerView.hidden = NO;
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertView.tag = 111;
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertView.tag = 111;
            [alertView show];
        }

    }];
}




-(void)loadViewDetails
{
    self.bikeInfoView.hidden = NO;
    self.priceView.hidden = NO;
    self.bottomView.hidden = NO;
    self.makeImageView.hidden = NO;
    self.locationTableView.hidden = YES;
    if (self.bikeDetailsObj.availabelAreaArray.count<2) {
        self.locationDropDownImage.hidden = YES;
    }
    else
    {
        self.locationDropDownImage.hidden = NO;
        [self.locationTableView changeWidthTo:self.view.frame.size.width-40 andHeightTo:10 + self.bikeDetailsObj.availabelAreaArray.count*30];
        [self.locationTableView changedXAxisTo:self.view.center.x-self.locationTableView.frame.size.width/2 andYAxisTo:self.bikeInfoView.frame.origin.y+self.locationView.frame.origin.y+20];
        [self.locationTableView reloadData];
        [self.view bringSubviewToFront:self.locationTableView];
    }
    WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:selectedArea];
    self.bikeNameLabel.text = self.bikeDetailsObj.bikeName;
    self.bikeDescriptionLabel.text = self.bikeDetailsObj.bikeDescription;
    self.locationLabel.text =[NSString stringWithFormat:@"Available at %@", area.areaName];
    [self.paramsDict setObject:area.area_id forKey:@"area_id"];
    
    self.effectivePriceLabel.text = area.priceWeekdays; //[NSString stringWithFormat:@"Effective per hour price : Rs.%@/-",[area.effectivePerHourPrice getFormattedPrice]];
    self.rentalPriceLabel.text = area.priceWeekEnd; //[NSString stringWithFormat:@"Rental price : Rs.%@/-",[area.rentalAmount getFormattedPrice]];
    self.makeImageView.contentMode = UIViewContentModeScaleToFill;
    NSString *imageUrl = [NSString stringWithFormat:@"%@", self.bikeDetailsObj.bikeMakeLogo];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.makeImageView;
    
    [self.makeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         self.makeImageView.contentMode = UIViewContentModeScaleAspectFit;
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
    
    CGFloat descriptionHeight = self.bikeDescriptionLabel.frame.size.height;
    CGFloat textHeight = [self returnBikeDescriptionTextSize];

    if ([self.showMoreButton.titleLabel.text isEqualToString:@"Show more"]) {
        if (textHeight<descriptionHeight) {
            self.showMoreButton.hidden = YES;
        }
        else
        {
            self.showMoreButton.hidden = NO;
        }
    }
    else
    {
        self.showMoreButton.hidden = NO;
    }
    [self.view bringSubviewToFront:self.backButton];
    [self.view bringSubviewToFront:self.locationTableView];
    [self.locationTableView reloadData];
    
    [self logEventsForBikeViewed];
}


#pragma mark - QGraphLoggingEventsForBikeViewed

-(void)logEventsForBikeViewed
{
    
    NSMutableDictionary *bikeDetails = [[NSMutableDictionary alloc] init];
    [bikeDetails setObject:self.bikeDetailsObj.bikeId forKey:@"bike_id"];
    [bikeDetails setObject:self.bikeDetailsObj.bikeName forKey:@"bike_name"];
    
    WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:selectedArea];
    
    [bikeDetails setObject:area.pricePerDay forKey:@"bike_price_per_hour"];
    
    [[QGSdk getSharedInstance] logEvent:@"bike_viewed" withParameters:bikeDetails];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}


-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bikeDetailsObj.availabelAreaArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = AVENIR_BOOK(15.0);
    cell.textLabel.tag = indexPath.row;
    cell.textLabel.textColor = [UIColor colorWithRed:0.64 green:0.64 blue:0.64 alpha:1];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:indexPath.row];
    cell.textLabel.text = area.areaName;
    if (area.isBikeAvailabel == NO) {
        cell.textLabel.alpha = 0.5;
    }
    return cell;
}

//-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 30.0;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WRArea *area = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:indexPath.row];
    if (area.isBikeAvailabel) {
        selectedArea = (int)indexPath.row;
        [self loactionViewTapped];
        
        [self loadViewDetails];
    }
}

#pragma mark - WRBikeDetailBookNowTableViewCellDelegate Methods

-(void)bookNowButtonTappedWithDaysSelected:(NSInteger)daysSelected
{
    WRReservationViewController *reservationVC = [self.storyboard instantiateViewControllerWithIdentifier:kReservationViewControllerID];
    [self.navigationController pushViewController:reservationVC animated:YES];
}

#pragma mark - UIButton Actions

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bookNowButtonAction:(id)sender
{
    if (![self.userManager hasPreviousLoggedInUser])
    {
        [self logEventsForBikeNow:@"GoToLogin"];
        WRLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:kLoginViewControllerID];
        loginVC.delegate = self;
        loginVC.isFromBiking = YES;
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    else
    {
         
        [self logEventsForBikeNow:@"GoToReservation"];
        
        WRReservationViewController *reservationVC = [self.storyboard instantiateViewControllerWithIdentifier:kReservationViewControllerID];
        reservationVC.paramsDict = self.paramsDict;
        reservationVC.selectedBike = self.selectedBikeObj;
        reservationVC.selectedArea = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:selectedArea];
        [self.navigationController pushViewController:reservationVC animated:YES];
    }
}

#pragma mark - QGraphLoggingEventsForBookNow

-(void)logEventsForBikeNow:(NSString*)category
{
    
    NSMutableDictionary *bikeDetails = [[NSMutableDictionary alloc] init];
    
    
    [bikeDetails setObject:category forKey:@"category"];
    
    [[QGSdk getSharedInstance] logEvent:@"book_now" withParameters:bikeDetails];
}




-(void)loactionViewTapped
{
    [self.containerView bringSubviewToFront:self.locationTableView];

    if (self.bikeDetailsObj.availabelAreaArray.count>1) {
        if (self.locationTableView.hidden == YES)
        {
            self.locationTableView.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.locationDropDownImage.transform = CGAffineTransformMakeRotation(M_PI);
            }];
        }
        else
        {
            self.locationTableView.hidden = YES;
            [UIView animateWithDuration:0.3 animations:^{
                self.locationDropDownImage.transform = CGAffineTransformMakeRotation(-0.0);
            }];
        }
    }
}

- (IBAction)showMoreButtonAction:(id)sender
{
    static BOOL isShowMoreButtonSelected = YES;
    if (isShowMoreButtonSelected) {
        [self.showMoreButton setTitle:@"Show less" forState:UIControlStateNormal];
        CGFloat descriptionHeight = self.bikeDescriptionLabel.frame.size.height;
        CGFloat textHeight = [self returnBikeDescriptionTextSize];
        
        if (textHeight<descriptionHeight) {
        }
        else
        {
            [self.containerView changeHeightTo:self.containerView.frame.size.height + (textHeight - descriptionHeight)+10];
            self.mainScrollView.contentSize = CGSizeMake(self.containerView.frame.size.width, self.containerView.frame.size.height);
        }
    }
    else
    {
        [self.showMoreButton setTitle:@"Show more" forState:UIControlStateNormal];
        if (IS_IPHONE_5_OR_BELLOW) {
            [self.containerView changeHeightTo:468.0];
        }
        else
        {
            [self.containerView changeHeightTo:self.view.frame.size.height-100];
        }
        self.mainScrollView.contentSize = CGSizeMake(self.containerView.frame.size.width, self.containerView.frame.size.height);

    }
    isShowMoreButtonSelected = !isShowMoreButtonSelected;
}


-(CGFloat )returnBikeDescriptionTextSize
{
    NSDictionary *attributes = @{NSFontAttributeName: AVENIR_BOOK(13.0)};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.bikeDetailsObj.bikeDescription attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-145, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    return size.height;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WRLoginViewControllerDelegate Methods

-(void)loginSuccessLoadNextViewController
{
    WRReservationViewController *reservationVC = [self.storyboard instantiateViewControllerWithIdentifier:kReservationViewControllerID];
    reservationVC.paramsDict = self.paramsDict;
    reservationVC.selectedBike = self.selectedBikeObj;
    reservationVC.selectedArea = [self.bikeDetailsObj.availabelAreaArray objectAtIndex:selectedArea];
    [self.navigationController pushViewController:reservationVC animated:YES];
}

#pragma mark -- YSLTransitionAnimatorDataSource
- (UIImageView *)popTransitionImageView
{
    return self.bikeImageView;
}

- (UIImageView *)pushTransitionImageView
{
    return nil;
}

#pragma mark - UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 111) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
