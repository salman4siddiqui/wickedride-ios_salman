//
//  WRBikeDetailsViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 20/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBike.h"

@interface WRBikeDetailsViewController : WRBaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *bikeImageView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (nonatomic, strong) WRBike *selectedBikeObj;

@property (nonatomic, strong) NSMutableDictionary *paramsDict;
@end
