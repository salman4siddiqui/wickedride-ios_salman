//
//  WRBikeDetailsTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 21/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeDetailsTableViewCell.h"
#import "WRConstants.h"

@interface WRBikeDetailsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *bikeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;


@end

@implementation WRBikeDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureCellWithBikeDetails:(WRBikeDetails *)bikeDetailsObj;
{
    self.bikeNameLabel.text = bikeDetailsObj.bikeName;
    self.locationLabel.text = [bikeDetailsObj.bikeLocationDict objectForKey:@"area"];
    self.descriptionLabel.text = nullCheck(bikeDetailsObj.bikeDescription);
}

#pragma mark - UIButton/ Tap Actions

- (IBAction)showMoreButtonAction:(id)sender
{
    
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
