//
//  WRThankYouViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 31/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@interface WRThankYouViewController : WRBaseViewController

@property (nonatomic, strong) NSMutableDictionary *bookingDict;

@end
