//
//  WRReservationViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRBike.h"
#import "WRArea.h"

@interface WRReservationViewController : WRBaseViewController

@property (nonatomic, strong) NSMutableDictionary *paramsDict;
@property (nonatomic, strong) WRBike *selectedBike;
@property (nonatomic, strong) WRArea *selectedArea;
@end
