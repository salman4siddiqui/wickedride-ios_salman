//
//  WRSummaryTermsTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 21/04/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRSummaryTermsTableViewCell.h"

@implementation WRSummaryTermsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


#pragma mark - Tap/Button action
- (IBAction)ageLimitButtonAction:(id)sender
{

    static BOOL isAgeLimitSelected = YES;
    if (isAgeLimitSelected) {
        [self.ageLimiButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
    }
    else
    {
        [self.ageLimiButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }
    [self.delegate ageLimitButtonTappedWithBool:isAgeLimitSelected];
    isAgeLimitSelected = !isAgeLimitSelected;
}

- (IBAction)drivingApproveButtonAction:(id)sender
{
    static BOOL isDrivingApprovalSelected = YES;
    if (isDrivingApprovalSelected) {
        [self.drivingApproveButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
    }
    else
    {
        [self.drivingApproveButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }
    [self.delegate drivingLicenceButtonTappedWithBool:isDrivingApprovalSelected];
    
    isDrivingApprovalSelected = !isDrivingApprovalSelected;
}

- (IBAction)termsAcceptButtonAction:(id)sender
{
    static BOOL isTermsSelected = YES;
    if (isTermsSelected) {
        
        [self.termsAcceptButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateNormal];
    }
    else
    {
        [self.termsAcceptButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }
    [self.delegate termsConditionButtonTappedWithBool:isTermsSelected];
    isTermsSelected = !isTermsSelected;
}
- (IBAction)termsAndConditionsButtonAction:(id)sender
{
    [self.delegate termsAndCondtionButtonTapped];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
