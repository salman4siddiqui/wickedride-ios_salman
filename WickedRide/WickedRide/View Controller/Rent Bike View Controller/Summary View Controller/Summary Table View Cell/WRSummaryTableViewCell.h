//
//  WRSummaryTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRArea.h"

@interface WRSummaryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *toDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

-(void)configureCellWithSelectedData:(NSMutableDictionary *)userInfo andSelectedArea:(WRArea *)area;
@end
