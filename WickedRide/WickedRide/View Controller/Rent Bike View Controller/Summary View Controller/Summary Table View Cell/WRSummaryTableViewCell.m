//
//  WRSummaryTableViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRSummaryTableViewCell.h"
#import "NSString+DataValidator.h"

@implementation WRSummaryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureCellWithSelectedData:(NSMutableDictionary *)userInfo andSelectedArea:(WRArea *)area
{
    self.fromDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[userInfo objectForKey:@"start_date"] convertDateIntoRequiredBikationString],[userInfo objectForKey:@"start_time"]];
    self.toDateLabel.text = [NSString stringWithFormat:@"%@\n%@",[[userInfo objectForKey:@"end_date"] convertDateIntoRequiredBikationString],[userInfo objectForKey:@"end_time"]];
    self.locationLabel.text = area.areaName;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
