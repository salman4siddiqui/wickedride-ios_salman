//
//  WRSummaryTermsTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 21/04/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRSummaryTermsTableViewCellDelegate <NSObject>

@optional

-(void)ageLimitButtonTappedWithBool:(BOOL )isSelected;
-(void)drivingLicenceButtonTappedWithBool:(BOOL )isSelected;
-(void)termsConditionButtonTappedWithBool:(BOOL )isSelected;
-(void)termsAndCondtionButtonTapped;

@end

@interface WRSummaryTermsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *ageLimiButton;
@property (weak, nonatomic) IBOutlet UIButton *drivingApproveButton;
@property (weak, nonatomic) IBOutlet UIButton *termsAcceptButton;

@property (nonatomic,weak) id <WRSummaryTermsTableViewCellDelegate> delegate;

@end
