//
//  WRBikeAvailabilityCalendarViewController.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeAvailabilityCalendarViewController.h"
#import "WRBikeAvailability.h"
#import "WRClockTableViewCell.h"
#import "WRTimeSlot.h"
#import "WRClockViewController.h"

#define kMonday @"M";
#define kTueday @"T";
#define kWednesday @"W";
#define kThursday @"T";
#define kFriday @"F";
#define kSaturday @"S";
#define kSunday @"S";

@interface WRBikeAvailabilityCalendarViewController ()<WRClockViewControllerDelegate>
{
    NSInteger month;
    NSInteger currentMonth;
    NSInteger currentYear;
    NSInteger year;
    NSInteger tag;
}
@property (weak, nonatomic) IBOutlet UIView *dateTitleView;
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (nonatomic, strong) WRBikeAvailability *bikeAvailability;
@property (nonatomic, strong) NSMutableArray *selectedCellArray;

@property (nonatomic, strong) NSMutableArray *avalabilityArray;
@property (nonatomic, strong) NSString *lastDateStr;
@property (nonatomic, strong) WRTimeSlot *lastTimeSlot;


@property (nonatomic, strong) UIScrollView *calenderScrollView;

@property (nonatomic, strong) UIButton *nextMonthButton;
@property (nonatomic, strong) UIButton *previousMonthsButton;

@property (nonatomic, strong) NSMutableArray *availableDateArray;
@end

@implementation WRBikeAvailabilityCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.calenderScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-200)];
    [self.view addSubview:self.calenderScrollView];
    self.navigationController.navigationBarHidden = YES;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    month = [components month];
    currentMonth = [components month];
    year = [components year];
    currentYear = [components year];
    
    if (self.paramsDict == nil) {
        [self loadCustomNavigationViewWithTitle:@"PICK UP DATE"];
        [self callWebServiceForFromDateAvailabilityCalenderInfo];
    }
    else
    {
        [self loadCustomNavigationViewWithTitle:@"DROP DATE"];
        self.isPickDateSelected = YES;
        [self.paramsDict removeObjectForKey:@"end_date"];
        [self.paramsDict removeObjectForKey:@"end_time"];
        NSString *startDateStr = [self.paramsDict objectForKey:@"start_date"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *startDate = [dateFormat dateFromString:startDateStr];
        
        NSDateComponents *monthComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:startDate];
        month = [monthComponents month];
        currentMonth = month;

        NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:startDate];
        year = [yearComponents year];
        currentYear = year;

        
        NSString *timeStr = [NSString stringWithFormat:@"%@",[[self.paramsDict objectForKey:@"start_time"] substringToIndex:5]];
        [self.paramsDict setObject:[[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:@"id"] forKey:@"city_id"];
        [self.paramsDict setObject:timeStr forKey:@"start_time"];
        [self.paramsDict setObject:self.selectedArea.area_id forKey:@"area_id"];
        [self.paramsDict setObject:self.selectedBike.bikeId forKey:@"model_id"];
        
        [self getToAvailabilityCalender];
    }
    
}

#pragma mark Create a Custom Calender for Available date

-(void)customiseCalenderView
{
    tag = 0;
    for (UIView *view in self.calenderScrollView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat width = (self.view.frame.size.width-70)/7;
    CGFloat xAxis = 20;
    for (int i = 0; i<7; i++) {
        CGRect rect = CGRectMake(xAxis, 0, width, width);
        [self.calenderScrollView addSubview:[self returnViewWithAppropriateDay:i andFrame:rect]];
        xAxis+=width+5;
    }
//    if (month == currentMonth && year == currentYear) {
//        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
//        [comp setDay:1];
//        
//        NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
//        
//        NSCalendar* cal = [NSCalendar currentCalendar];
//        NSDateComponents* daysComp = [cal components:kCFCalendarUnitWeekday fromDate:firstDayOfMonthDate];
//        
//        NSInteger weekday = [daysComp weekday]-1;
//        NSString *dateStr = [NSString stringWithFormat:@"%@-%ld-%ld",@"1",(long)month,(long)year];
//        
//        // Convert string to date object
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        [dateFormat setDateFormat:@"dd-MM-yyyy"];
//        NSDate *date = [dateFormat dateFromString:dateStr];
//        NSCalendar *c = [NSCalendar currentCalendar];
//        NSRange days = [c rangeOfUnit:NSDayCalendarUnit
//                               inUnit:NSMonthCalendarUnit
//                              forDate:date];
//        
//        NSString* dateOnlyAsString = [[[NSDate date] description] convertDateIntoStringForClock];
//        NSInteger todayDate = [[dateOnlyAsString substringToIndex:2] integerValue];
//        
//        CGFloat dateXAxis = (weekday*width)+weekday*5 +20;
//        CGFloat dateYAxis = width+20;
//        for (int date = 1 ; date<=days.length; date++)
//        {
//            CGRect rect = CGRectMake(dateXAxis, dateYAxis, width, width);
//            [self.calenderScrollView addSubview:[self returnAvailableDateViewDate:date andFrame:rect andTodayDate:todayDate]];
//            dateXAxis+=width+5;
//            if (dateXAxis+width>self.view.frame.size.width) {
//                dateXAxis = 20;
//                dateYAxis +=width+5;
//            }
//        }
//        [self addNextAndPreviousButtonWithYAxis:dateYAxis+width];
//    }
//    else
    {
        NSString *dateStr = [NSString stringWithFormat:@"%@-%ld-%ld",@"1",(long)month,(long)year];
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSDate *date = [dateFormat dateFromString:dateStr];

        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
        [comp setDay:1];
        NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
        
        NSCalendar* cal = [NSCalendar currentCalendar];
        NSDateComponents* daysComp = [cal components:kCFCalendarUnitWeekday fromDate:firstDayOfMonthDate];
        
        NSInteger weekday = [daysComp weekday]-1;
        NSCalendar *c = [NSCalendar currentCalendar];
        NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                               inUnit:NSMonthCalendarUnit
                              forDate:date];
        
        CGFloat dateXAxis = (weekday*width)+weekday*5 +20;
        CGFloat dateYAxis = width+20;
        for (int date = 1 ; date<=days.length; date++)
        {
            CGRect rect = CGRectMake(dateXAxis, dateYAxis, width, width);
            [self.calenderScrollView addSubview:[self returnAvailableDateViewDate:date andFrame:rect andTodayDate:1]];
            dateXAxis+=width+5;
            if (dateXAxis+width>self.view.frame.size.width) {
                dateXAxis = 20;
                dateYAxis +=width+5;
            }
        }
        [self addNextAndPreviousButtonWithYAxis:dateYAxis+width];
    }
    [self changeNextAndPreviousButtonText];
}


#pragma mark Add next/ previous Button to calender scroll view

-(void)addNextAndPreviousButtonWithYAxis:(CGFloat )yAxis
{
    self.previousMonthsButton = [[UIButton alloc] init];
    self.previousMonthsButton.backgroundColor = [UIColor blackColor];
    [self.previousMonthsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.previousMonthsButton.titleLabel.font = MONTSERRAT_BOLD(15.0);
    [self.previousMonthsButton addTarget:self action:@selector(previousMonthsButtonTapped) forControlEvents:UIControlEventTouchDown];
    [self.calenderScrollView addSubview:self.previousMonthsButton];
    
    self.nextMonthButton = [[UIButton alloc] init];
    self.nextMonthButton.backgroundColor = [UIColor blackColor];
    [self.nextMonthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.nextMonthButton.titleLabel.font = MONTSERRAT_BOLD(15.0);
    [self.nextMonthButton addTarget:self action:@selector(nextMonthsButtonTapped) forControlEvents:UIControlEventTouchDown];
    [self.calenderScrollView addSubview:self.nextMonthButton];
    
    CGFloat centreX = self.calenderScrollView.center.x;
    self.previousMonthsButton.translatesAutoresizingMaskIntoConstraints = YES;
    self.previousMonthsButton.frame = CGRectMake(centreX-120, yAxis+10, 100, 50);
    self.nextMonthButton.translatesAutoresizingMaskIntoConstraints = YES;
    self.nextMonthButton.frame = CGRectMake(centreX+20, yAxis+10, 100, 50);
    self.calenderScrollView.contentSize = CGSizeMake(self.view.frame.size.width, yAxis+100);
    
    if (month == currentMonth && year == currentYear) {
        self.previousMonthsButton.userInteractionEnabled = NO;
        self.previousMonthsButton.alpha = 0.2;
    }
    else
    {
        self.previousMonthsButton.userInteractionEnabled = YES;
        self.previousMonthsButton.alpha = 1.0;
    }
    // Convert string to date object
    if (self.lastDateStr !=nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:self.lastDateStr];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        if ([components month] == month && [components year] == year ) {
            self.nextMonthButton.userInteractionEnabled = NO;
            self.nextMonthButton.alpha = 0.2;
        }
        else
        {
            self.nextMonthButton.userInteractionEnabled = YES;
            self.nextMonthButton.alpha = 1.0;
        }

    }


}

#pragma mark Return Days View

-(UIView *)returnViewWithAppropriateDay:(int )day andFrame:(CGRect )rect;
{
    CGFloat width = (self.view.frame.size.width-70)/7;

    UIView *daysView = [[UIView alloc] initWithFrame:rect];
//    daysView.backgroundColor = [UIColor cyanColor];
    UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, width)];
    dayLabel.font = MONTSERRAT_BOLD(15.0);
    dayLabel.textAlignment = NSTextAlignmentCenter;
    switch (day) {
        case WRDaysMonday:
            dayLabel.text = kMonday;
            break;
        case WRDaysTuesday:
            dayLabel.text = kTueday;
            break;
        case WRDaysWednesday:
            dayLabel.text = kWednesday;
            break;
        case WRDaysThursday:
            dayLabel.text = kThursday;
            break;
        case WRDaysFriday:
            dayLabel.text = kFriday;
            break;
        case WRDaysSaturday:
            dayLabel.text = kSaturday;
            break;
        case WRDaysSunday:
            dayLabel.text = kSunday;
            break;
            
        default:
            break;
    }
    
    
    [daysView addSubview:dayLabel];
    
    
    return daysView;
}

#pragma mark Return Dates View

-(UIView *)returnAvailableDateViewDate:(int )date andFrame:(CGRect )rect andTodayDate:(NSInteger )todayDate
{
    CGFloat width = (self.view.frame.size.width-70)/7;
    
    UIView *dateView = [[UIView alloc] initWithFrame:rect];
//    dateView.backgroundColor = [UIColor orangeColor];
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, width)];
    dateLabel.font = AVENIR_BOOK(15.0);
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.text = [NSString stringWithFormat:@"%d",date];
    
    if ([self.availableDateArray containsObject:[NSString stringWithFormat:@"%d",date]]) {
        tag++;
        dateView.userInteractionEnabled = YES;
        dateView.alpha = 1.0;
        dateView.tag = tag;
        UITapGestureRecognizer *dateSelectedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateViewTapped:)];
        [dateView addGestureRecognizer:dateSelectedTap];
    }
    else
    {
        dateView.userInteractionEnabled = NO;
        dateView.alpha = 0.4;
    }
    dateView.layer.cornerRadius = dateView.frame.size.width/2;
    dateView.clipsToBounds = YES;
    
    [dateView addSubview:dateLabel];
    return dateView;
}

#pragma mark - Call Web service

-(void)callWebServiceForFromDateAvailabilityCalenderInfo
{
    self.selectedCellArray = [NSMutableArray array];
    self.paramsDict =[NSMutableDictionary dictionaryWithDictionary:@{@"model_id":self.selectedBike.bikeId,
                                                                     @"area_id":self.selectedArea.area_id,
                                                                     @"month":[NSNumber numberWithInteger:month],
                                                                     @"year":[NSNumber numberWithInteger:year],
                                                                     @"city_id":[[[NSUserDefaults standardUserDefaults] objectForKey:kSelectedLocation] objectForKey:@"id"]}];

    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aFromDateAvailabiltyCalender andParameters:self.paramsDict withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        [self generateAvailabilityValuesFrom:[[responseDictionary objectForKey:@"result"] objectForKey:@"data"]];
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }

    }];
    
}

#pragma mark Create Date Array Availability Value

-(void)createAndSetDateArrayValue
{
    self.availableDateArray = [NSMutableArray array];
    for (WRBikeAvailability *availability in self.avalabilityArray) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:availability.dateStr];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:date];
        NSInteger day = [components day];
        [self.availableDateArray addObject:[NSString stringWithFormat:@"%ld",(long)day]];
    }
    
    [self customiseCalenderView];
    [self changeDateLabelTextWithMonthsAndYear];
}

-(void)generateAvailabilityValuesFrom:(NSMutableArray *)array
{
    self.avalabilityArray = [NSMutableArray array];
    for (NSDictionary *availabilityDict in array) {
        if ([[availabilityDict objectForKey:@"status"] isEqualToString:@"A"]||[[availabilityDict objectForKey:@"status"] isEqualToString:@"P"]) {
            if ([[availabilityDict objectForKey:@"slots"] isKindOfClass:[NSArray class]]) {
                WRBikeAvailability *availability = [WRBikeAvailability getBikeAvailabilityInfoFrom:availabilityDict];
                [self.avalabilityArray addObject:availability];
            }
        }
    }
    [self createAndSetDateArrayValue];
}

#pragma mark - UIButton/Tap actions

-(void)previousMonthsButtonTapped
{
    month--;
    if (month == 0) {
        month = 12;
        year--;
    }
    if (self.isPickDateSelected) {
        NSString *dateStr;
        int day;
        if (month == currentMonth && year == currentYear) {
            dateStr = [self.paramsDict objectForKey:@"start_date"];
            day = [[[self.paramsDict objectForKey:@"start_date"] substringFromIndex:8] intValue];
        }
        else
        {
            dateStr = [NSString stringWithFormat:@"%ld-%ld-01",(long)year,(long)month];
            day = 1;
        }

        [self createDropDateArray:[self returnDateFromString:dateStr] andSelectedDay:day];
    }
    else
    {
        [self callWebServiceForFromDateAvailabilityCalenderInfo];
    }

}

-(void)nextMonthsButtonTapped
{
    month++;
    if (month>12) {
        month = 1;
        year++;
    }
    
    if (self.isPickDateSelected) {
        NSString *dateStr = [NSString stringWithFormat:@"%ld-%ld-01",(long)year,(long)month];

        [self createDropDateArray:[self returnDateFromString:dateStr] andSelectedDay:1];
    }
    else
    {
        [self callWebServiceForFromDateAvailabilityCalenderInfo];
    }
}


-(void)dateViewTapped:(UIGestureRecognizer *)gesture
{
    NSInteger selectedIndex = gesture.view.tag;
    UIView *selectedDateView = [self.calenderScrollView viewWithTag:selectedIndex];
    
    for (UIView *view in self.calenderScrollView.subviews) {
        if (![view isKindOfClass:[UIButton class]]) {
            if (view.tag == selectedDateView.tag) {
                for (UILabel *dateLabel in view.subviews) {
                    view.backgroundColor = [UIColor blackColor];
                        dateLabel.textColor = [UIColor whiteColor];
                    }
            }
            else
            {
                for (UILabel *dateLabel in view.subviews) {
                    view.backgroundColor = [UIColor whiteColor];
                    dateLabel.textColor = [UIColor blackColor];
                }
            }
        }
    }
    
    self.bikeAvailability = [self.avalabilityArray objectAtIndex:selectedIndex-1];
    if (!self.isPickDateSelected) {
        if (self.bikeAvailability!=nil) {
            //            self.customNavView.navTitleLabel.text = @"CHOOSE DROP DATE AND TIME";
            //            [self.nextButton setTitle:@"DONE" forState:UIControlStateNormal];
            //            [self.paramsDict removeObjectForKey:@"month"];
            //            [self.paramsDict removeObjectForKey:@"year"];
            //            [self.paramsDict setObject:self.bikeAvailability.dateStr forKey:@"start_date"];
            //            NSIndexPath *index = [self.selectedCellArray objectAtIndex:0];
            //            WRTimeSlot *slot = [self.bikeAvailability.slotsArray objectAtIndex:index.row];
            //            [self.paramsDict setObject:slot.startTime forKey:@"start_time"];
            //            [self.selectedCellArray removeAllObjects];
            //            [self getToAvailabilityCalender];
            //            [self.availabilityTableView reloadData];
            //
            //            currentMonth = month;
            //            currentYear = year;
            //            self.previousMonthButton.enabled = NO;
            //            self.isPickDateSelected = YES;
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.bikeAvailability = self.bikeAvailability;
            clockVC.selectedArea = self.selectedArea;
            clockVC.selectedBike = self.selectedBike;
            clockVC.delegate = self;
            clockVC.isFromBikeAvailability = YES;
            [self.navigationController pushViewController:clockVC animated:YES];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select your pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else
    {
        if (self.bikeAvailability!=nil) {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.bikeAvailability = self.bikeAvailability;
            clockVC.selectedArea = self.selectedArea;
            clockVC.selectedBike = self.selectedBike;
            clockVC.isPickUpDateSelected = YES;
            clockVC.isFromBikeAvailability = YES;
            clockVC.paramsDict = self.paramsDict;
            clockVC.lastDateStr = self.lastDateStr;
            clockVC.lastTimeSlot = self.lastTimeSlot;
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select your drop date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }

}


- (IBAction)previosMonthButtonAction:(id)sender {
    month--;
    if (month == 0) {
        month = 12;
        year--;
    }
    [self customiseCalenderView];
    [self changeDateLabelTextWithMonthsAndYear];
}

- (IBAction)nextMonthButtonAction:(id)sender
{
    month++;
    if (month>12) {
        month = 1;
        year++;
    }
    [self customiseCalenderView];
    [self changeDateLabelTextWithMonthsAndYear];
}
-(void)changeDateLabelTextWithMonthsAndYear
{
    switch (month) {
        case 1:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"January %ld",(long)year];
            break;
        case 2:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"February %ld",(long)year];
            break;
        case 3:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"March %ld",(long)year];
            break;
        case 4:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"April %ld",(long)year];
            break;
        case 5:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"May %ld",(long)year];
            break;
        case 6:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"June %ld",(long)year];
            break;
        case 7:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"July %ld",(long)year];
            break;
        case 8:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"August %ld",(long)year];
            break;
        case 9:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"September %ld",(long)year];
            break;
        case 10:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"October %ld",(long)year];
            break;
        case 11:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"November %ld",(long)year];
            break;
        case 12:
            self.dateTitleLabel.text = [NSString stringWithFormat:@"December %ld",(long)year];
            break;
            
        default:
            break;
    }
}

-(void)changeNextAndPreviousButtonText
{
    switch (month) {
        case 1:
            [self.previousMonthsButton setTitle:@"DEC" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"FEB" forState:UIControlStateNormal];
            break;
        case 2:
            [self.previousMonthsButton setTitle:@"JAN" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"MAR" forState:UIControlStateNormal];
            break;
        case 3:
            [self.previousMonthsButton setTitle:@"FEB" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"APR" forState:UIControlStateNormal];
            break;
        case 4:
            [self.previousMonthsButton setTitle:@"MAR" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"MAY" forState:UIControlStateNormal];
            break;
        case 5:
            [self.previousMonthsButton setTitle:@"APR" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"JUN" forState:UIControlStateNormal];
            break;
        case 6:
            [self.previousMonthsButton setTitle:@"MAY" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"JUL" forState:UIControlStateNormal];
            break;
        case 7:
            [self.previousMonthsButton setTitle:@"JUN" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"AUG" forState:UIControlStateNormal];
            break;
        case 8:
            [self.previousMonthsButton setTitle:@"JUL" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"SEP" forState:UIControlStateNormal];
            break;
        case 9:
            [self.previousMonthsButton setTitle:@"AUG" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"OCT" forState:UIControlStateNormal];
            break;
        case 10:
            [self.previousMonthsButton setTitle:@"SEP" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"NOV" forState:UIControlStateNormal];
            break;
        case 11:
            [self.previousMonthsButton setTitle:@"OCT" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"DEC" forState:UIControlStateNormal];
            break;
        case 12:
        {
            [self.previousMonthsButton setTitle:@"NOV" forState:UIControlStateNormal];
            [self.nextMonthButton setTitle:@"JAN" forState:UIControlStateNormal];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)nextButtonAction:(id)sender
{
    if (!self.isPickDateSelected) {
        if (self.bikeAvailability!=nil) {
//            self.customNavView.navTitleLabel.text = @"CHOOSE DROP DATE AND TIME";
//            [self.nextButton setTitle:@"DONE" forState:UIControlStateNormal];
//            [self.paramsDict removeObjectForKey:@"month"];
//            [self.paramsDict removeObjectForKey:@"year"];
//            [self.paramsDict setObject:self.bikeAvailability.dateStr forKey:@"start_date"];
//            NSIndexPath *index = [self.selectedCellArray objectAtIndex:0];
//            WRTimeSlot *slot = [self.bikeAvailability.slotsArray objectAtIndex:index.row];
//            [self.paramsDict setObject:slot.startTime forKey:@"start_time"];
//            [self.selectedCellArray removeAllObjects];
//            [self getToAvailabilityCalender];
//            [self.availabilityTableView reloadData];
//            
//            currentMonth = month;
//            currentYear = year;
//            self.previousMonthButton.enabled = NO;
//            self.isPickDateSelected = YES;
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.bikeAvailability = self.bikeAvailability;
            clockVC.selectedArea = self.selectedArea;
            clockVC.selectedBike = self.selectedBike;
            clockVC.delegate = self;
            clockVC.isFromBikeAvailability = YES;
            [self.navigationController pushViewController:clockVC animated:YES];

        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select your pick up date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else
    {
        if (self.bikeAvailability!=nil) {
            WRClockViewController *clockVC = [self.storyboard instantiateViewControllerWithIdentifier:kClockViewControllerID];
            clockVC.bikeAvailability = self.bikeAvailability;
            clockVC.selectedArea = self.selectedArea;
            clockVC.selectedBike = self.selectedBike;
            clockVC.isPickUpDateSelected = YES;
            clockVC.isFromBikeAvailability = YES;
            clockVC.delegate = self;
            [self.navigationController pushViewController:clockVC animated:YES];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select your drop date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

-(void)getToAvailabilityCalender
{
    self.selectedCellArray = [NSMutableArray array];
    [self loadMBProgressCustomLoaderView];
    [self.networkManager startGETRequestWithAPI:aToDateAvailabiltyCalender andParameters:self.paramsDict withSuccess:^(NSDictionary *responseDictionary) {
        [self hideMBProgressCutomLoader];
        self.lastDateStr = [[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"end_date"];
        if (self.lastDateStr == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[responseDictionary objectForKey:@"result"] objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            self.lastTimeSlot = [WRTimeSlot getTimeSlotInfoFrom:[[[responseDictionary objectForKey:@"result"] objectForKey:@"data"] objectForKey:@"last_slot"]];
            
            [self createDropDateArray:[self returnDateFromString:[self.paramsDict objectForKey:@"start_date"]] andSelectedDay:[[[self.paramsDict objectForKey:@"start_date"] substringFromIndex:8] intValue]];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [self hideMBProgressCutomLoader];
        if (![errorMessage isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

-(void)createDropDateArray:(NSDate *)startDate andSelectedDay:(int )selectedDay
{
    NSMutableArray *dropDateArray = [NSMutableArray array];
//    NSDate *today = [self returnDateFromStrimg:[self.paramsDict objectForKey:@"start_date"]]; //Get a date object for today's date
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:startDate];
    for (int day = selectedDay; day<=days.length; day++) {
        NSString *dayStr;
        if (day<10) {
            dayStr = [NSString stringWithFormat:@"0%d",day];
        }
        else
        {
            dayStr = [NSString stringWithFormat:@"%d",day];
        }
        
        NSString *monthStr;
        if (month<10) {
            monthStr = [NSString stringWithFormat:@"0%ld",(long)month];
        }
        else
        {
            monthStr = [NSString stringWithFormat:@"%ld",(long)month];
        }

        NSString *dateStr = [NSString stringWithFormat:@"%ld-%@-%@",(long)year,monthStr,dayStr];
        
        if ([[self.paramsDict objectForKey:@"start_date"] isEqualToString:self.lastDateStr]) {
            int endTime = [[self.lastTimeSlot.endTime substringToIndex:2] intValue];
            NSMutableArray *timeSlotArray = [NSMutableArray array];
            for (int hour = [[[self.paramsDict objectForKey:@"start_time"] substringToIndex:2] intValue]+1; hour<endTime; hour++) {
                NSDictionary *slotDict = @{@"end_time":[NSString stringWithFormat:@"%d:00:00",hour+1],
                                           @"id":[NSNumber numberWithInt:hour],
                                           @"start_time":[NSString stringWithFormat:@"%d:00:00",hour],
                                           @"status":@1};
                
                [timeSlotArray addObject:slotDict];
            }
            NSDictionary *availabilityDict = @{@"date":dateStr,
                                               @"slots":timeSlotArray,
                                               @"status":@"A"};
            [dropDateArray addObject:availabilityDict];
            day = (int)days.length;
        }
        else if ([dateStr isEqualToString:[self.paramsDict objectForKey:@"start_date"]]) {
            NSMutableArray *timeSlotArray = [NSMutableArray array];
            int startTime = [[[self.paramsDict objectForKey:@"start_time"] substringToIndex:2] intValue]+1;
            if (startTime < 23) {
                for (int hour = startTime; hour<23; hour++) {
                    
                    NSDictionary *slotDict = @{@"end_time":[NSString stringWithFormat:@"%d:00:00",hour+1],
                                               @"id":[NSNumber numberWithInt:hour],
                                               @"start_time":[NSString stringWithFormat:@"%d:00:00",hour],
                                               @"status":@1};
                    
                    [timeSlotArray addObject:slotDict];
                }
                NSDictionary *availabilityDict = @{@"date":dateStr,
                                                   @"slots":timeSlotArray,
                                                   @"status":@"A"};
                [dropDateArray addObject:availabilityDict];
            }
        }
        
        else if ([dateStr isEqualToString:self.lastDateStr]) {
            int endTime = [[self.lastTimeSlot.endTime substringToIndex:2] intValue];
            NSMutableArray *timeSlotArray = [NSMutableArray array];
            for (int hour = 9; hour<endTime; hour++) {
                NSDictionary *slotDict = @{@"end_time":[NSString stringWithFormat:@"%d:00:00",hour+1],
                                           @"id":[NSNumber numberWithInt:hour],
                                           @"start_time":[NSString stringWithFormat:@"%d:00:00",hour],
                                           @"status":@1};
                
                [timeSlotArray addObject:slotDict];
            }
            NSDictionary *availabilityDict = @{@"date":dateStr,
                                               @"slots":timeSlotArray,
                                               @"status":@"A"};
            [dropDateArray addObject:availabilityDict];
            day = (int)days.length;
        }
        else
        {
            NSMutableArray *timeSlotArray = [NSMutableArray array];
            for (int hour = 9; hour<23; hour++) {
                NSDictionary *slotDict = @{@"end_time":[NSString stringWithFormat:@"%d:00:00",hour+1],
                                           @"id":[NSNumber numberWithInt:hour],
                                           @"start_time":[NSString stringWithFormat:@"%d:00:00",hour],
                                           @"status":@1};
                
                [timeSlotArray addObject:slotDict];
            }
            NSDictionary *availabilityDict = @{@"date":dateStr,
                                               @"slots":timeSlotArray,
                                               @"status":@"A"};
            [dropDateArray addObject:availabilityDict];
        }
    }
    [self generateAvailabilityValuesFrom:dropDateArray];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WRClockViewControllerDelegate Method

-(void)selectedStartTime:(NSString *)timeStr
{
    
}

-(void)selectedEndDateAndTimeDict:(NSDictionary *)endDateDict
{
    NSString *startTime = [NSString stringWithFormat:@"%@",[self.paramsDict objectForKey:@"start_time"]];
    [self.paramsDict setObject:startTime forKey:@"start_time"];
    [self.paramsDict addEntriesFromDictionary:endDateDict];    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBikeAvailableDateSelected object:nil userInfo:self.paramsDict];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
