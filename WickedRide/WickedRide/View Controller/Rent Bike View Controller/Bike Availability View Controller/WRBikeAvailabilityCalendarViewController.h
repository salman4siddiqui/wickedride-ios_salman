//
//  WRBikeAvailabilityCalendarViewController.h
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"
#import "WRArea.h"
#import "WRBike.h"

typedef enum
{
    WRDaysSunday,
    WRDaysMonday,
    WRDaysTuesday,
    WRDaysWednesday,
    WRDaysThursday,
    WRDaysFriday,
    WRDaysSaturday
    
}WRDaysList;


@protocol WRBikeAvailabilityCalendarViewControllerDelegate <NSObject>

@optional

-(void)bikeAvailabilityWithSelctedDate:(NSMutableDictionary *)params;

@end

@interface WRBikeAvailabilityCalendarViewController : WRBaseViewController

@property (nonatomic, strong) WRArea *selectedArea;
@property (nonatomic, strong) WRBike *selectedBike;
@property (nonatomic, assign) BOOL isPickDateSelected;
@property (nonatomic, strong) NSMutableDictionary *paramsDict;

@property (nonatomic, assign) id <WRBikeAvailabilityCalendarViewControllerDelegate> delegate;

@end
