//
//  WRBikeListingCollectionViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBike.h"

@interface WRBikeListingCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bikeImageView;
@property (weak, nonatomic) IBOutlet UILabel *soldOutLabel;
@property (weak, nonatomic) IBOutlet UIImageView *soldOutImg;

-(void)configureCellWithBikeListing:(WRBike *)bikeObj;

@end
