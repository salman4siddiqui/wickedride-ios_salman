//
//  WRBikeListingCollectionViewCell.m
//  WickedRide
//
//  Created by Ajith Kumar on 11/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeListingCollectionViewCell.h"
#import "WRArea.h"
#import "NSString+DataValidator.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "UIView+FrameChange.h"

@interface WRBikeListingCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *perDayLabel;
@property (weak, nonatomic) IBOutlet UIView *effectivePriceView;
@property (weak, nonatomic) IBOutlet UILabel *effectiveNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *effectiveLocationView;
@property (weak, nonatomic) IBOutlet UILabel *effectiveWeekendLabel;
@property (weak, nonatomic) IBOutlet UILabel *effectiveWeekdayLabel;


@end

@implementation WRBikeListingCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
//    self.contentView.layer.cornerRadius = 5.0;
//    self.contentView.clipsToBounds = YES;
//    self.contentView.layer.borderWidth = 2.0;
//    self.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    [self.contentView.layer setShadowColor:[UIColor blackColor].CGColor];
//    [self.contentView.layer setShadowOpacity:0.9];
//    [self.contentView.layer setShadowRadius:10.0];
//    [self.contentView.layer setShadowOffset:CGSizeMake(0.0, 10.0)];
}

-(void)configureCellWithBikeListing:(WRBike *)bikeObj
{
    WRArea *areaObj = [bikeObj.availableAreaArray objectAtIndex:0];
    if (areaObj.effectivePrice != nil) {
        self.nameLabel.text = bikeObj.bikeName;
        self.rentAmountLabel.text =[NSString stringWithFormat:@"Rs.%@/-", [areaObj.totalAmount getFormattedPrice]];
      //  self.perDayLabel.text = [NSString stringWithFormat:@"Rs.%@/hour",[areaObj.effectivePrice getFormattedPrice]];
        
        self.perDayLabel.text = areaObj.priceWeekEnd;
        
        self.effectiveWeekendLabel.text = areaObj.priceWeekEnd;
        self.effectiveWeekdayLabel.text = areaObj.priceWeekdays;
        
//        if (bikeObj.availableAreaArray.count>1) {
//            if (bikeObj.availableAreaArray.count == 2) {
//                self.locationLabel.text = [NSString stringWithFormat:@"%@ & %lu place",areaObj.areaName,bikeObj.availableAreaArray.count-1];
//            }
//            else
//            {
//                self.locationLabel.text = [NSString stringWithFormat:@"%@ & %lu places",areaObj.areaName,bikeObj.availableAreaArray.count-1];
//            }
//        }
//        else
//        {
//            self.locationLabel.text = areaObj.areaName;
//        }
        int availableCount = 0;
        NSString *locationName;
        
        if (bikeObj.availableAreaArray.count) {
            for (WRArea *area in bikeObj.availableAreaArray) {
                if (area.isBikeAvailabel) {
                    availableCount++;
                    if (locationName == nil || [locationName isEmptyString]) {
                        locationName = area.areaName;
                    }
                }
            }
        }
        else
        {
            self.locationLabel.hidden = YES;
        }
        if (availableCount > 1) {
            if (availableCount > 2) {
                self.locationLabel.text = [NSString stringWithFormat:@"%@ & %d places",locationName,availableCount-1];
            }
            else
            {
                self.locationLabel.text = [NSString stringWithFormat:@"%@ & 1 place",locationName];
            }
        }
        else
        {
            self.locationLabel.text = locationName;
        }

        self.effectivePriceView.hidden = YES;
    }
    else
    {
        self.effectiveNameLabel.text = bikeObj.bikeName;

        self.effectivePriceView.hidden = NO;
        if (bikeObj.availableAreaArray.count>1) {
            if (bikeObj.availableAreaArray.count == 2) {
                self.effectiveLocationView.text = [NSString stringWithFormat:@"%@ & %lu place",areaObj.areaName,bikeObj.availableAreaArray.count-1];
            }
            else
            {
                self.effectiveLocationView.text = [NSString stringWithFormat:@"%@ & %lu places",areaObj.areaName,bikeObj.availableAreaArray.count-1];
            }

        }
        else
        {
            self.effectiveLocationView.text = areaObj.areaName;
        }

        self.effectiveWeekdayLabel.text =areaObj.priceWeekdays;
        self.effectiveWeekendLabel.text = areaObj.priceWeekEnd;
    }
    
    NSString *imageUrl = [bikeObj.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.bikeImageView;
    self.bikeImageView.contentMode = UIViewContentModeScaleToFill;
    [self.bikeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
         self.bikeImageView.contentMode = UIViewContentModeScaleAspectFit;
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
    
    float aspectRatio = 172.0/335.0; //cell height/width;

    [self.bikeImageView changeWidthTo:self.frame.size.width andHeightTo:self.frame.size.width*aspectRatio];
    
//    [self.bikeImageView changeHeightTo:self.frame.size.width*aspectRatio];
    if (bikeObj.isSoldOut) {
        self.soldOutLabel.hidden = NO;
        self.soldOutImg.hidden = NO;
    }
    else
    {
        self.soldOutLabel.hidden = YES;
        self.soldOutImg.hidden = YES;
    }


}

@end
