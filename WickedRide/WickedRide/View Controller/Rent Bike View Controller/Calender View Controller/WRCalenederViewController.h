//
//  WRCalenederViewController.h
//  MediaPlayerDemo
//
//  Created by Ajith Kumar on 08/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@protocol WRCalenederViewControllerDelegate <NSObject>

@optional

-(void)nextButtonTappedWithPickUpDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr;
-(void)nextButtonTappedWithDropDate:(NSDate *)selectedDate andTheTime:(NSString *)timeStr;

@end

@interface WRCalenederViewController : WRBaseViewController

@property (nonatomic, assign) BOOL isFromFilter;
@property (nonatomic, assign) BOOL isFromBikeListing;
@property (nonatomic, assign) BOOL isPickUpDateSelected;

@property (nonatomic, strong) NSDate *pickDate;
@property (nonatomic, strong) NSString *pickDateStartTime;

@property (nonatomic, strong) NSMutableDictionary *paramsDict;

@property (nonatomic, weak) id <WRCalenederViewControllerDelegate>delegate;
@end
