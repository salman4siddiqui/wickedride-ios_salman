//
//  WRCalendarTableViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 19/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WRClockTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;

@end
