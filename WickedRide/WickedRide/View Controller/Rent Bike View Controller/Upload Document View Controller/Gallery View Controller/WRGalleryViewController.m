//
//  WRGalleryViewController.m
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRGalleryViewController.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@interface WRGalleryViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic,strong) UIScrollView *imageScrollView;

@end

@implementation WRGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadImageScrollView];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButton/tap actions

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteButtonAction:(id)sender
{
    int page = self.imageScrollView.contentOffset.x/self.view.frame.size.width;
    NSString *docId = nullCheck([[self.imageArray objectAtIndex:page] objectForKey:@"doc_id"]);
    
    if (![docId isEmptyString] && docId != nil) {
        [self deletePhotosFromGalleryWithDocId:docId];
    }
}


#pragma mark - Load Scroll View 

-(void)loadImageScrollView
{
    self.imageScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.imageScrollView.delegate = self;
    CGFloat x= 0;
    self.pageControl.numberOfPages = self.imageArray.count;
    self.imageScrollView.pagingEnabled = YES;

    for (NSDictionary *imageDict in self.imageArray) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, -20, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[imageDict objectForKey:@"url"]]];
        
        __weak UIImageView *weakImg = imageView;
        
        [imageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
         }];
        
        [self.imageScrollView addSubview:imageView];
        x+=self.imageScrollView.frame.size.width;
    }
    self.imageScrollView.contentSize = CGSizeMake(x, 0);
    
    [self.view addSubview:self.imageScrollView];
    [self.view bringSubviewToFront:self.backButton];
    [self.view bringSubviewToFront:self.pageControl];
    [self.view bringSubviewToFront:self.deleteButton];
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x/self.view.frame.size.width;
    self.pageControl.currentPage = page;
}

#pragma mark - Delete Photos from list

-(void)deletePhotosFromGalleryWithDocId:(NSString *)docId
{
    
    [self loadMBProgressCustomLoaderView];
    [self.networkManager POST:aDeleteUploadedDocument parameters:@{@"doc_id":docId} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideMBProgressCutomLoader];
        [self.delegate imageDeletedSuccessfully];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideMBProgressCutomLoader];
        if (![error.localizedDescription isEqualToString:kErrorMessage]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Something went Wrong. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
