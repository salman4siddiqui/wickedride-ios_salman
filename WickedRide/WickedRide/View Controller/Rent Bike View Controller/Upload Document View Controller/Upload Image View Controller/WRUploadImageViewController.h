//
//  WRUploadImageViewController.h
//  WickedRide
//
//  Created by Apple on 25/12/16.
//  Copyright © 2016 Inkoniq. All rights reserved.
//

#import "WRBaseViewController.h"

@protocol  WRUploadImageViewControllerDelegate<NSObject>

@optional

-(void)imageUploadedSuccessfully;

@end

@interface WRUploadImageViewController : WRBaseViewController

@property (nonatomic, assign) BOOL isFromLicence;
@property (nonatomic, weak) id <WRUploadImageViewControllerDelegate> delegate;

@end
