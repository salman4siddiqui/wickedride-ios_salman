//
//  WRBikationBrandCollectionViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 12/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRBikeMake.h"

@interface WRBikesBrandCollectionViewCell : UICollectionViewCell

-(void)configureBeandCellWith:(WRBikeMake *)bikeMakeObj;

@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;

@end
