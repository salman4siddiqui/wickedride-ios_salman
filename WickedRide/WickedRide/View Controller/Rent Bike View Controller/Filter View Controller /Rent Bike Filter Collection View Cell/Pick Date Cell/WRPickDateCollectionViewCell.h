//
//  WRPickDateCollectionViewCell.h
//  WickedRide
//
//  Created by Ajith Kumar on 18/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WRPickDateCollectionViewCellDelegate <NSObject>

@optional

-(void)pickUpBikeCalenderTapped;
-(void)dropBikeCalenderTapped;

@end

@interface WRPickDateCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *pickUpDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropDateLabel;

@property (nonatomic, weak) id <WRPickDateCollectionViewCellDelegate> delegate;

-(void)configureViewWithSelectedDates:(NSMutableDictionary *)dataDict;
@end
