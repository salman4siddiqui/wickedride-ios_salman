//
//  UIView+FrameChange.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FrameChange)

- (void)changedXAxisTo:(CGFloat )xAxis;
- (void)changedYAxisTo:(CGFloat )yAxis;
- (void)changedXAxisTo:(CGFloat )xAxis andYAxisTo:(CGFloat )yAxis;

- (void)changedYAxisTo:(CGFloat)yAxis andHeightTo:(CGFloat )height;
- (void)changedXAxisTo:(CGFloat)xAxis andWidthTo:(CGFloat)width;


- (void)changeWidthTo:(CGFloat )width;
- (void)changeHeightTo:(CGFloat )height;
- (void)changeWidthTo:(CGFloat )width andHeightTo:(CGFloat )height;


-(void)addTapGestureWithTarget:(id)target action:(SEL)callbackFunc;
@end
