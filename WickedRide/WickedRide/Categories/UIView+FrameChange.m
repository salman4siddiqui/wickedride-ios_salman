//
//  UIView+FrameChange.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "UIView+FrameChange.h"

@implementation UIView (FrameChange)

- (void)changedXAxisTo:(CGFloat )xAxis
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(xAxis, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (void)changedYAxisTo:(CGFloat)yAxis
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(self.frame.origin.x, yAxis, self.frame.size.width, self.frame.size.height);
}

- (void)changedXAxisTo:(CGFloat)xAxis andYAxisTo:(CGFloat)yAxis
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(xAxis, yAxis, self.frame.size.width, self.frame.size.height);
}

- (void)changedXAxisTo:(CGFloat)xAxis andWidthTo:(CGFloat)width
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(xAxis, self.frame.origin.y, width, self.frame.size.height);
}

- (void)changedYAxisTo:(CGFloat)yAxis andHeightTo:(CGFloat )height
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(self.frame.origin.x, yAxis, self.frame.size.width, height);
}

- (void)changeWidthTo:(CGFloat)width
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height);
}

- (void)changeHeightTo:(CGFloat)height
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
}

- (void)changeWidthTo:(CGFloat)width andHeightTo:(CGFloat)height
{
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, width, height);
}


#pragma mark - Gestures

-(void)addTapGestureWithTarget:(id)target action:(SEL)callbackFunc
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:callbackFunc];
    [self addGestureRecognizer:tapGesture];
}



@end
