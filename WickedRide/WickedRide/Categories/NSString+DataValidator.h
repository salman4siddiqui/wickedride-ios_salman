//
//  NSString+DataValidator.h
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DataValidator)
- (BOOL)validateEmail;
- (BOOL)validatePhoneNumber;
- (BOOL)isEmptyString;

-(NSString *)convertMiliSecondToDate;
-(NSString *)returnValidPriceInIndianCurrencyFormat;
-(NSString *)convertDateIntoString;
- (NSString *)getFormattedPrice;
-(NSString *)convertDateIntoStringForClock;
-(NSString *)convertDateIntoRequiredBikationString;

-(NSString *)convertDateIntoStringForBirthdayDate;

@end
