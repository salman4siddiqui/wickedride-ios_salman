//
//  NSString+DataValidator.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "NSString+DataValidator.h"

@implementation NSString (DataValidator)

- (BOOL)validateEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    
    return [emailPred evaluateWithObject:self];
}

- (BOOL)validatePhoneNumber
{
    BOOL isPhoneNumber = NO;
    //Validate phone using NSTextCheckingResult
    NSError *error = NULL;
    
    //Assign Data detector with NSTextCheckingType
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSString *numberWithoutSpace = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Set range
    NSRange inputRange = NSMakeRange(0, [numberWithoutSpace length]);
    NSArray *matches = [detector matchesInString:numberWithoutSpace options:0 range:inputRange];
    
    if([matches count] == 0)
    {
        isPhoneNumber = NO;
    }
    
    else
    {
        //Found match , check if matches the whole string
        NSTextCheckingResult *result= (NSTextCheckingResult *)[matches objectAtIndex:0];
        
        if([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length)
        {
            //Matched whole thing
            isPhoneNumber = YES;
        }
        else
        {
            isPhoneNumber = NO;
        }
    }
    
    return isPhoneNumber;
}



- (BOOL)isEmptyString
{
    BOOL isEmp = NO;
    
    if([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        isEmp = YES;
    }
    else
    {
        isEmp = NO;
    }
    
    return isEmp;
}


-(NSString *)convertMiliSecondToDate
{
    NSString *dateStr = self;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dateStr doubleValue]/1000];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM YYYY | hh:mma"];
    
    return [dateFormatter stringFromDate:date];
}


-(NSString *)returnValidPriceInIndianCurrencyFormat
{
    float currency = [self floatValue];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:currency]];
    return numberAsString;
}

-(NSString *)convertDateIntoString
{
    NSString *dateStr = self;
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    NSRange range = [dateStr rangeOfString:@" "];
    
    NSString *newString = [dateStr substringToIndex:range.location];
    NSLog(@"%@",newString);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:newString];
    
    //Optionally for time zone conversions
    NSString *stringFromDate = [dateFormatter stringFromDate:date];
    //unless ARC is active
    return stringFromDate;
}

-(NSString *)convertDateIntoStringForBirthdayDate
{
    NSString *dateStr = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    
    //Optionally for time zone conversions
    NSString *stringFromDate = [formatter stringFromDate:date];
    //unless ARC is active
    return stringFromDate;
}

-(NSString *)convertDateIntoStringForClock
{
    NSString *dateStr = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy"];
    
    NSRange range = [dateStr rangeOfString:@" "];
    
    NSString *newString = [dateStr substringToIndex:range.location];
    NSLog(@"%@",newString);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:newString];
    
    //Optionally for time zone conversions
    NSString *stringFromDate = [formatter stringFromDate:date];
    //unless ARC is active
    return stringFromDate;
}

-(NSString *)convertDateIntoRequiredBikationString
{
    NSString *dateStr = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy"];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    
    //Optionally for time zone conversions
    NSString *stringFromDate = [formatter stringFromDate:date];
    //unless ARC is active
    return stringFromDate;
}


- (NSString *)getFormattedPrice
{
    // Number formatter with decimal style to show commas
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    NSString *formattedPrice = [formatter stringFromNumber:[NSNumber numberWithFloat:[self floatValue]]];
    
    return formattedPrice;
}


@end
