//
//  WREventsDetailView.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/12/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WREventsDetailView.h"
#import "NSString+DataValidator.h"

@implementation WREventsDetailView

-(void)awakeFromNib
{
    [super awakeFromNib];
    UITapGestureRecognizer *attendedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attendedViewTapped)];
    [self.BookingStatusLabel addGestureRecognizer:attendedTap];
}

-(void)configureViewWithBookingDetails:(WRBikation *)bikation andIsAttended:(BOOL )isAttended;
{
    self.bikationObj = bikation;
    if (bikation == nil) {
        self.noEventsView.hidden = NO;
    }
    else
    {
        if (isAttended) {
            self.BookingStatusLabel.text = [@"Attended" uppercaseString];
            self.BookingStatusLabel.userInteractionEnabled = YES;
        }
        else
        {
            self.BookingStatusLabel.text = [@"Confirmed" uppercaseString];
            self.BookingStatusLabel.userInteractionEnabled = NO;
        }
        self.noEventsView.hidden = YES;
        self.eventNameLabel.text = [NSString stringWithFormat:@"%@ (%@)",bikation.bikationTitle,bikation.distance];
        self.eventConductorLabel.text = bikation.conductorName;
        self.dateLabel.text = [NSString stringWithFormat:@"%@ (%@)",[bikation.startDate convertDateIntoRequiredBikationString],bikation.duration];
        self.locationLabel.text = bikation.startCity;
        
        self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[bikation.price getFormattedPrice]];
    }
}

#pragma mark UIButton/ Tap Action

-(void)attendedViewTapped
{
    [self.delegate attendedLabelTappedWithSelectedBikation:self.bikationObj];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
