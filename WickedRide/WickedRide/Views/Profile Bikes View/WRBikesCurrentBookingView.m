//
//  WRBikesCurrentBookingView.m
//  WickedRide
//
//  Created by Ajith Kumar on 31/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikesCurrentBookingView.h"
#import "WRAccessories.h"
#import "NSString+DataValidator.h"

@interface WRBikesCurrentBookingView ()

@property (weak, nonatomic) IBOutlet UILabel *bookingDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpLocation;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *noEventView;

@end

@implementation WRBikesCurrentBookingView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)configureViewWithBookingDetails:(WRBooking *)bookingObj
{
    if (bookingObj == nil) {
        self.noEventView.hidden = NO;
    }
    else
    {
        NSString *titleStr = bookingObj.bikeObj.bikeName;
//        for (WRAccessories *accessory in bookingObj.accessoriesArray) {
//            titleStr = [NSString stringWithFormat:@"%@+%@",titleStr,accessory.name];
//        }
        self.bookingDetailsLabel.text = titleStr;
        if (![bookingObj.pickUpTime isEmptyString]) {
            NSString *timeStr = [bookingObj.pickUpTime substringToIndex:2];
            NSString *suffixStr;
            int time = [timeStr intValue];
            if (time>12) {
                time-=12;
                suffixStr = @"PM";
            }
            else
            {
                suffixStr = @"AM";
            }
            self.pickUpTimeLabel.text = [NSString stringWithFormat:@"%@ - %d:00 %@",[bookingObj.pickUpDate convertDateIntoRequiredBikationString],time,suffixStr];
        }
        self.pickUpLocation.text = bookingObj.area.areaName;
        self.addressLabel.text = bookingObj.area.address;
        self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/-",[bookingObj.price getFormattedPrice]];
        self.noEventView.hidden = YES;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
