//
//  ALCalenderPopUpView.h
//  Aisle
//
//  Created by Ajith Kumar on 09/04/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  WRCalenderViewDelegate <NSObject>

@optional

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date;

@end

@interface WRCalenderPopUpView : UIView

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (nonatomic, weak) id<WRCalenderViewDelegate>delegate;

-(void)configureViewWithType:(NSString *)pickerType;

@end
