//
//  ALCalenderPopUpView.m
//  Aisle
//
//  Created by Ajith Kumar on 09/04/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCalenderPopUpView.h"

@interface WRCalenderPopUpView ()
{
    NSInteger index;
}
@property (nonatomic, strong) NSString *typeStr;
@property (nonatomic, strong) NSMutableArray *weightArray;
@property (nonatomic, strong) NSArray *heightArray;
@end

@implementation WRCalenderPopUpView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 3.0;
    self.clipsToBounds = YES;

    // Minimum and maximun date in date picker
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:-110];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    
    [comps setYear:0];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    
    [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];

    self.datePicker.minimumDate = minDate;
    self.datePicker.maximumDate = maxDate;

}

#pragma mark Custom Methods


-(void)configureViewWithType:(NSString *)pickerType
{
        self.datePicker.hidden = NO;
}

- (IBAction)doneButtonAction:(id)sender
{
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyy-MM-dd"];
        NSString *selectedDate =[df stringFromDate:self.datePicker.date];
        NSString *currentDate = [df stringFromDate:[NSDate date]];
        if ([selectedDate isEqualToString:currentDate])
        {
            NSString *selectedDate =[df stringFromDate:self.datePicker.maximumDate];
            [self.delegate doneButtonTappedWithSelectedDate:selectedDate andDate:self.datePicker.maximumDate];
        }
        else
        {
            [self.delegate doneButtonTappedWithSelectedDate:selectedDate andDate:self.datePicker.date];
        }
}



@end
