//
//  WRAccessoryView.m
//  WickedRide
//
//  Created by Ajith Kumar on 24/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRAccessoryView.h"
#import "WRConstants.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@implementation WRAccessoryView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.nameLabel.font = MONTSERRAT_REGULAR(18.0);
    self.priceLabel.font = AVENIR_ROMAN(17.0);
}

-(void)configureViewWithAccesspry:(WRAccessories *)accessories
{
    NSString *imageUrl = [accessories.imageDict objectForKey:@"full"];
    UIImage *placeholderImg = [UIImage imageNamed:@"place_holder"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
    
    __weak UIImageView *weakImg = self.accessoryImageView;
    
    [self.accessoryImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
         });
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];
    self.nameLabel.text = accessories.name;
    self.priceLabel.text = [NSString stringWithFormat:@"Rs. %@/hour",accessories.rentalAmount];;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
