//
// RSDFDatePickerMonthHeader.m
//
// Copyright (c) 2013 Evadne Wu, http://radi.ws/
// Copyright (c) 2013-2015 Ruslan Skorb, http://ruslanskorb.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "RSDFDatePickerMonthHeader.h"
@interface RSDFDatePickerMonthHeader ()


@property (nonatomic, readonly, strong) NSCalendar *calendar;

@end
@implementation RSDFDatePickerMonthHeader

@synthesize dateLabel = _dateLabel;
@synthesize calendar = _calendar;
@synthesize daysOfWeekView = _daysOfWeekView;

#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInitializer];
    }
    return self;
}

- (NSCalendar *)calendar
{
    if (!_calendar) {
        _calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        _calendar.locale = [NSLocale currentLocale];
    }
    return _calendar;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInitializer];
    }
    return self;
}

- (void)commonInitializer
{
    [self addSubview:[self daysOfWeekView]];
    self.backgroundColor = [self selfBackgroundColor];
}

- (RSDFDatePickerDaysOfWeekView *)daysOfWeekView
{
    if (!_daysOfWeekView) {
        _daysOfWeekView = [[[self daysOfWeekViewClass] alloc] initWithFrame:[self daysOfWeekViewFrame] calendar:self.calendar];
        [_daysOfWeekView layoutIfNeeded];
    }
    return _daysOfWeekView;
}

- (Class)daysOfWeekViewClass
{
    return [RSDFDatePickerDaysOfWeekView class];
}


- (CGRect)daysOfWeekViewFrame
{
    BOOL isPhone = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
    BOOL isPortraitInterfaceOrientation = UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]);
    
    CGRect namesOfDaysViewFrame = self.bounds;
    if (isPhone) {
        if (isPortraitInterfaceOrientation) {
            namesOfDaysViewFrame.origin.y = 50.0f;
            namesOfDaysViewFrame.size.height = 40.0f;
        } else {
            namesOfDaysViewFrame.size.height = 26.0f;
        }
    } else {
        namesOfDaysViewFrame.size.height = 36.0f;
    }
    
    return namesOfDaysViewFrame;
}


#pragma mark - Custom Accessors

- (UILabel *)dateLabel
{
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.opaque = NO;
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        _dateLabel.font = [self monthLabelFont];
        _dateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_dateLabel];
    }
    return _dateLabel;
}

- (void)setDate:(RSDFDatePickerDate)date
{
    _date = date;
}

- (void)setCurrentMonth:(BOOL)currentMonth
{
    _currentMonth = currentMonth;
    if (!_currentMonth) {
        self.dateLabel.textColor = [self monthLabelTextColor];
    } else {
        self.dateLabel.textColor = [self currentMonthLabelTextColor];
    }
    self.dateLabel.frame = CGRectMake(0, 15, self.bounds.size.width, 30);
}

#pragma mark - Attributes of the View

- (UIColor *)selfBackgroundColor
{
    return [UIColor clearColor];
}

#pragma mark - Attributes of Subviews

- (UIFont *)monthLabelFont
{
    return [UIFont fontWithName:@"Montserrat-Bold" size:12.0f];
}

- (UIColor *)monthLabelTextColor
{
    return [UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:39.0/255.0 alpha:1.0];
}

- (UIColor *)currentMonthLabelTextColor
{
    return [UIColor blackColor];
}

@end
