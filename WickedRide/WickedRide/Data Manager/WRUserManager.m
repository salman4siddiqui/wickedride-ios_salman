//
//  WRUserManager.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRUserManager.h"
#import "WRConstants.h"

@implementation WRUserManager
+ (WRUserManager *)sharedManager
{
    __strong static id _manager = nil;
    
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        
        _manager = [[self alloc] init];
        
    });
    
    
    return _manager;
}

-(void)setUser:(WRUser *)user
{
    if(_user != user && user != nil)
    {
        //Assign user
        _user = user;
        
        // set YES to isLoggedIn
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_USER_LOGGED_IN];
        
        // Archive this new user to "loggedInUser"
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:self.user];
        [[NSUserDefaults standardUserDefaults] setObject:archivedData forKey:IS_LOGGED_IN_USER_DETAILS];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (WRUser *)unArchivedUser
{
    if (_unArchivedUser == nil)
    {
        NSData *unArchiveData = [[NSUserDefaults standardUserDefaults] objectForKey:IS_LOGGED_IN_USER_DETAILS];
        _unArchivedUser = (WRUser *) [NSKeyedUnarchiver unarchiveObjectWithData:unArchiveData];
    }
    
    return _unArchivedUser;
}

- (BOOL)hasPreviousLoggedInUser
{
    BOOL has = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:IS_LOGGED_IN_USER_DETAILS])
    {
        if (self.user == nil)
        {
            self.user = self.unArchivedUser;
        }
        
        has = YES;
    }
    else
    {
        has = NO;
    }
    
    return has;
    
}

- (void)logOutUser
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:IS_LOGGED_IN_USER_DETAILS];
    self.user = nil;
    self.unArchivedUser = nil;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
