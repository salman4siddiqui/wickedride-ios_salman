//
//  main.m
//  WickedRide
//
//  Created by Ajith Kumar on 10/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WRAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WRAppDelegate class]));
    }
}
