//
//  WRCustomLoader.m
//  WickedRide
//
//  Created by Ajith Kumar on 28/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRCustomLoader.h"

@implementation WRCustomLoader

+(WRCustomLoader *)sharedInstance
{
    
    static WRCustomLoader * single=nil;
    
    @synchronized(self)
    {
        if(!single)
        {
            single = [[WRCustomLoader alloc] init];
            [single CustomLoader];
        }
        
    }
    return single;
}

-(void)CustomLoader
{
//    NSMutableArray *images = [[NSMutableArray alloc] init];
//    NSInteger animationImageCount = 12;
//    
//    for (int i = 1; i <=animationImageCount; i++){
//        self.tag==2015 ? [images addObject:(id)[UIImage imageNamed:[NSString stringWithFormat:@"b%d.png", i]].CGImage] :[images addObject:(id)[UIImage imageNamed:[NSString stringWithFormat:@"g%d", i]].CGImage];
//    }
//    
//    
//    self.lastanimationImage = self.tag==2015 ? [UIImage imageNamed:[NSString stringWithFormat:@"b%ld", animationImageCount - 1]]:[UIImage imageNamed:[NSString stringWithFormat:@"g%ld", animationImageCount - 1]];
//    
//    
//    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"contents"];
//    animation.calculationMode = kCAAnimationDiscrete;
//    animation.duration = 3.0;
//    animation.values = images;
//    animation.repeatCount =INFINITY;
//    animation.delegate = self;
//    animation.removedOnCompletion = NO;
//    animation.fillMode = kCAFillModeForwards;
//    [self.layer addAnimation:animation forKey:@"animation"];

    _loaderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    NSMutableArray *imagesArray = [[NSMutableArray alloc]init];
    
    for (int i=1; i<=12; i++) {
        NSString *iconPath;
        if (i>=10) {
            iconPath = [NSString stringWithFormat:@"%d.png",i];
        }
        else
        {
            iconPath = [NSString stringWithFormat:@"0%d.png",i];
        }
        UIImage *image = [UIImage imageNamed:iconPath];
        [imagesArray addObject:image];
    }
    self.loaderImageView.animationImages = imagesArray;
    self.loaderImageView.animationDuration = 1.0f;
    self.loaderImageView.animationRepeatCount = INFINITY;
}

@end
