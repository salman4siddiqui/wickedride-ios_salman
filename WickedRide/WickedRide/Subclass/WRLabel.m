//
//  WRLabel.m
//  WickedRide
//
//  Created by Ajith Kumar on 19/08/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRLabel.h"
#import "WRConstants.h"

@implementation WRLabel

-(void)awakeFromNib
{
    [super awakeFromNib];
    NSString *string = self.text;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    float spacing;
//    if (self.tag == 1001) {
        if (IS_IPHONE_5_OR_BELLOW) {
            spacing = 0.3;
        }
        else
        {
            spacing = 0.6f;
        }
//    }
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    self.attributedText = attributedString;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
