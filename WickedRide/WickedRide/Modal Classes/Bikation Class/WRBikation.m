//
//  WRBikation.m
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikation.h"

@implementation WRBikation

+(WRBikation *)getBikationInformationFrom:(NSDictionary *)resultDict
{
    WRBikation *bikationObj = [WRBikation new];
    
    bikationObj.conductorId = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"conductor"] objectForKey:@"id"]];
    bikationObj.conductorName = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"conductor"] objectForKey:@"name"]];
    bikationObj.contactNumber = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"conductor"] objectForKey:@"contact_number"]];
    bikationObj.averageRating = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"conductor"] objectForKey:@"avg_rating"]];
    bikationObj.conductorImageDict = [[resultDict objectForKey:@"conductor"] objectForKey:@"image"];
    
    bikationObj.distance = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"distance"]];
    bikationObj.duration = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"duration"]];
    bikationObj.bikationId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    bikationObj.imageDict = [resultDict objectForKey:@"image"];

    NSString *newString = [[[resultDict objectForKey:@"price"] componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];

    bikationObj.price = newString;
    bikationObj.startCity = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"start_city"]];
    bikationObj.startDate = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"start_date"]];
    bikationObj.startTime = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"start_time"]];
    bikationObj.bikationTitle = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"title"]];

    return bikationObj;
}

@end
