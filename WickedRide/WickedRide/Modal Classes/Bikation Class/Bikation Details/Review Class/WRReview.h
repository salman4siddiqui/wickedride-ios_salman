//
//  WRReview.h
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRReview : NSObject

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *reviewId;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *review;
@property (nonatomic, strong) NSString *reviewTitle;
@property (nonatomic, strong) NSDictionary *reviewerImageDict;
@property (nonatomic, strong) NSString *reviewerId;
@property (nonatomic, strong) NSString *reviewerName;

+(WRReview *)getReviewInfoOfBikationFrom:(NSDictionary *)resultDict;
@end
