//
//  WRBikationDetails.h
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBikationDetails : NSObject


@property (nonatomic, strong) NSMutableArray *inclusionArray;
@property (nonatomic, strong) NSMutableArray *exculusionArray;
@property (nonatomic, strong) NSMutableArray *reviewArray;
@property (nonatomic, strong) NSMutableArray *recommendedBikes;
@property (nonatomic, strong) NSMutableArray *rideRules;

@property (nonatomic, strong) NSString *availableSlot;
@property (nonatomic, strong) NSString *bikationDescription;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *meetingPoint;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *totalSlots;

+(WRBikationDetails *)getBikationDetailedInformationFrom:(NSDictionary *)resultDict;

@end
