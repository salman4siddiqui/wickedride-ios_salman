//
//  WRFeatureInfo.m
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRFeatureInfo.h"

@implementation WRFeatureInfo

+(WRFeatureInfo *)getFeatureInfoOfBikationFrom:(NSDictionary *)resultDict
{
    WRFeatureInfo *featureInfo = [WRFeatureInfo new];
    featureInfo.featureDescription = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"description"]];
    featureInfo.iconImageDict = [resultDict objectForKey:@"icon"];
    return featureInfo;
}

@end
