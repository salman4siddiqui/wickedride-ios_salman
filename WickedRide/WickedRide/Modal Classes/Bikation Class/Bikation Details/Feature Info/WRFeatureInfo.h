//
//  WRFeatureInfo.h
//  WickedRide
//
//  Created by Ajith Kumar on 23/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRFeatureInfo : NSObject

@property (nonatomic, strong) NSString *featureDescription;
@property (nonatomic, strong) NSDictionary *iconImageDict;

+(WRFeatureInfo *)getFeatureInfoOfBikationFrom:(NSDictionary *)resultDict;

@end
