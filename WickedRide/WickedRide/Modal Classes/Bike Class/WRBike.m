//
//  WRBike.m
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBike.h"
#import "WRArea.h"

@implementation WRBike

+(WRBike *)getBikeInformationFrom:(NSDictionary *)resultDict
{
    WRBike *bikeObj = [WRBike new];
    
    bikeObj.bikeName = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"name"]];
    bikeObj.bikeId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    bikeObj.imageDict = [resultDict objectForKey:@"image"];
    
    bikeObj.availableAreaArray = [NSMutableArray array];
    for (NSDictionary *areaDict in  [resultDict objectForKey:@"available_locations"])
    {
        WRArea *area = [WRArea getAreaInformationFrom:areaDict];
        [bikeObj.availableAreaArray addObject:area];
    }
    if ([bikeObj.availableAreaArray count]) {
        NSMutableArray *array = [NSMutableArray array];
        for (WRArea *area in bikeObj.availableAreaArray) {
            if (area.isBikeAvailabel == NO) {
                [array addObject:area];
            }
        }
        if (array.count == bikeObj.availableAreaArray.count) {
            bikeObj.isSoldOut = YES;
        }
        else
        {
            bikeObj.isSoldOut = NO;
        }
    }
    else
    {
        bikeObj.isSoldOut = YES;
    }
    return bikeObj;
}


@end
