//
//  WRBikeAvailability.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeAvailability.h"
#import "WRTimeSlot.h"

@implementation WRBikeAvailability

+(WRBikeAvailability *) getBikeAvailabilityInfoFrom:(NSDictionary *)resultDict
{
    WRBikeAvailability *bikeAvailability = [WRBikeAvailability new];
    
    bikeAvailability.dateStr = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"date"]];
    bikeAvailability.slotsArray = [NSMutableArray array];
    for (NSDictionary *timeSlotDict in [resultDict objectForKey:@"slots"]) {
        
        if ([[timeSlotDict objectForKey:@"status"] boolValue]) {
            WRTimeSlot *timeSlot = [WRTimeSlot getTimeSlotInfoFrom:timeSlotDict];
            [bikeAvailability.slotsArray addObject:timeSlot];
        }
    }
    return bikeAvailability;
}

@end
