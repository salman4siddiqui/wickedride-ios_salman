//
//  WRRentSummary.m
//  WickedRide
//
//  Created by Ajith Kumar on 27/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRRentSummary.h"

@implementation WRRentSummary

+(WRRentSummary *)getRentSummaryInformationFrom:(NSDictionary *)resultDict
{
    WRRentSummary *rentSummary = [WRRentSummary new];
    rentSummary.rentAmount = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"rent_amount"]];
    rentSummary.numberOfDays = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"rent_number_of_days"]];
    rentSummary.rentSummaryId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"rent_summary_id"]];
    return rentSummary;
}

@end
