//
//  WRRentSummary.h
//  WickedRide
//
//  Created by Ajith Kumar on 27/10/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRRentSummary : NSObject
@property (nonatomic, strong) NSString *rentAmount;
@property (nonatomic, strong) NSString *numberOfDays;
@property (nonatomic, strong) NSString *rentSummaryId;

+(WRRentSummary *)getRentSummaryInformationFrom:(NSDictionary *)resultDict;

@end
