//
//  WRAccessories.m
//  WickedRide
//
//  Created by Ajith Kumar on 25/11/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRAccessories.h"

@implementation WRAccessories

+(WRAccessories *)getAccessoriesInformationFrom:(NSDictionary *)resultDict
{
    WRAccessories *accessoryObj = [WRAccessories new];
    accessoryObj.accessoryId = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    accessoryObj.name = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"name"]];
    accessoryObj.accessoryDescription = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"description"]];
    NSString *newString = [[[resultDict objectForKey:@"rental_amount"] componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    accessoryObj.rentalAmount = newString;
    accessoryObj.imageDict = [resultDict objectForKey:@"image"];
    return accessoryObj;
}

@end
