//
//  WRArea.h
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRArea : NSObject

@property (nonatomic, strong) NSString *areaName;
@property (nonatomic, strong) NSString *area_id;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *address;
//@property (nonatomic, strong) NSString *effectivePerHourPrice;
//@property (nonatomic, strong) NSString *pricePerHourWeekday;
//@property (nonatomic, strong) NSString *pricePerHourWeekend;
//@property (nonatomic, strong) NSString *rentalAmount;
@property (nonatomic, strong) NSString *priceWeekdays;
@property (nonatomic, strong) NSString *priceWeekEnd;

@property (nonatomic, strong) NSString *effectivePrice;
@property (nonatomic, strong) NSString *pricePerDay;
@property (nonatomic, strong) NSString *noOfHour;
@property (nonatomic, strong) NSString *totalAmount;
@property (nonatomic, strong) NSString *noOfDay;
@property (nonatomic, assign) BOOL isBikeAvailabel;

+(WRArea *)getAreaInformationFrom:(NSDictionary *)resultDict;

@end
