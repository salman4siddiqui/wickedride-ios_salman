//
//  WRBikeMake.m
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "WRBikeMake.h"

@implementation WRBikeMake

+(WRBikeMake *)getInformationOfBikeMakesFrom:(NSDictionary *)resultDict
{
    WRBikeMake *bikeMakeObj = [WRBikeMake new];
    
    bikeMakeObj.bikeMakeLogo = [NSString stringWithFormat:@"%@",[[resultDict objectForKey:@"logo"] objectForKey:@"full"]];
    bikeMakeObj.bikeModel = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"name"]];
//    bikeMakeObj.city_id = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"city_id"]];
    bikeMakeObj.make_id = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"id"]];
    bikeMakeObj.make_name = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"name"]];

    return bikeMakeObj;
}
@end
