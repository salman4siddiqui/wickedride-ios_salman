//
//  WRBikeMake.h
//  WickedRide
//
//  Created by Ajith Kumar on 22/09/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRBikeMake : NSObject

@property (nonatomic, strong) NSString *bikeModel;
@property (nonatomic, strong) NSString *bikeMakeLogo;
@property (nonatomic, strong) NSString *city_id;
@property (nonatomic, strong) NSString *make_id;
@property (nonatomic, strong) NSString *make_name;

+(WRBikeMake *)getInformationOfBikeMakesFrom:(NSDictionary *)resultDict;

@end
